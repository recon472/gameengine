// system
import AngularMovementComponent from "./Engine/GameComponents/AngularMovementComponent.js";
import HitCollisionComponent from "./Engine/GameComponents/HitCollisionComponent.js";

// user


export default {
    // system
    AngularMovementComponent: AngularMovementComponent,
    HitCollisionComponent: HitCollisionComponent,

    // user
    
}
