import gEngine from "../GameEngine.js";
import {
    Scene, Camera
} from "../GameEngine.js";

export default class EmptyLevel extends Scene {
    constructor () {
        super();

        this.camera = null;
    }

    windowResized () {
        gEngine.Core.setFullscreen();
        this.camera.setViewport([0, 0, window.innerWidth, window.innerHeight])
    }

    initialize () {
        this.camera = new Camera(
            vec2.fromValues(50, 37.5),
            100,
            [0, 0, 640, 480]
        );
        this.getGameWorld().addCamera(this.camera);
        this.windowResized();
    }

    update () {

    }

    loadScene () {

    }

    unloadScene () {

    }
}
