precision mediump float;

// object that fethes data from texture
uniform sampler2D uSampler;
uniform vec4 uPixelColor;

// shadow
#define kMaxShadowOpacity 0.7
#define kLightStrengthCutOff 0.05

// light information
#define ePointLight 0
#define eDirectionalLight 1
#define eSpotLight 2
struct Light {
    vec3 Position;
    vec3 Direction;
    vec4 Color;
    float Near;
    float Far;
    float CosInner;
    float CosOuter;
    float Intensity;
    float DropOff;
    bool IsOn;
    int LightType;
};
uniform Light uLights[1];
varying vec2 vTextureCoordinate;

float AngularDropOff(vec3 lightDirection, vec3 L) {
    float attenuation = 0.0;
    float cosL = dot(lightDirection, L);
    float number = cosL - uLights[0].CosOuter;
    if (number > 0.0) {
        if (cosL > uLights[0].CosInner) {
            attenuation = 1.0;
        } else {
            float denom = uLights[0].CosInner - uLights[0].CosOuter;
            attenuation = smoothstep(0.0, 1.0, pow(number / denom, uLights[0].DropOff));
        }
    }
    return attenuation;
}

float DistanceDropOff(float dist) {
    float attenuation = 0.0;
    if (dist <= uLights[0].Far) {
        if (dist <= uLights[0].Near) {
            attenuation = 1.0;
        } else {
            float n = dist - uLights[0].Near;
            float d = uLights[0].Far - uLights[0].Near;
            attenuation = smoothstep(0.0, 1.0, 1.0 - (n * n) / (d * d));
        }
    }
    return attenuation;
}

float LightStrength() {
    float aAttenuation = 1.0;
    float dAttenuation = 1.0;
    vec3 lightDirection = -normalize(uLights[0].Direction.xyz);
    vec3 L;
    float dist;
    if (uLights[0].LightType == eDirectionalLight) {
        L = lightDirection;
    } else {
        L = uLights[0].Position.xyz - gl_FragCoord.xyz;
        dist = length(L);
        L = L / dist;
    }
    if (uLights[0].LightType == eSpotLight) {
        aAttenuation = AngularDropOff(lightDirection, L);
    }
    if (uLights[0].LightType != eDirectionalLight) {
        dAttenuation = DistanceDropOff(dist);
    }
    float result = aAttenuation * dAttenuation;
    return result;
}

void main(void) {
    vec4 textureFragmentColor = texture2D(uSampler, vTextureCoordinate);
    float lightStrength = LightStrength();
    if (lightStrength < kLightStrengthCutOff) {
        discard;
    }
    vec3 shadowColor = lightStrength * uPixelColor.rgb;
    shadowColor *= uPixelColor.a * textureFragmentColor.a;
    gl_FragColor = vec4(shadowColor, kMaxShadowOpacity * lightStrength * textureFragmentColor.a);
}
