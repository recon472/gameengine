precision mediump float;

// object that fethes data from texture
uniform sampler2D uSampler;

// color information
uniform vec4 uPixelColor;
uniform vec4 uGlobalAmbientColor;
uniform float uGlobalAmbientIntensity;

// light information
#define kGLSLuLightArraySize 4
#define ePointLight 0
#define eDirectionalLight 1
#define eSpotlight 2
struct Light {
    vec3 Position;
    vec3 Direction;
    vec4 Color;
    float Near;
    float Far;
    float CosInner;
    float CosOuter;
    float Intensity;
    float DropOff;
    bool IsOn;
    int LightType;
};
uniform Light uLights[kGLSLuLightArraySize];
varying vec2 vTextureCoordinate;

float AngularDropOff(Light light, vec3 lightDirection, vec3 L) {
    float attenuation = 0.0;
    float cosL = dot(lightDirection, L);
    float number = cosL - light.CosOuter;
    if (number > 0.0) {
        if (cosL > light.CosInner) {
            attenuation = 1.0;
        } else {
            float denom = light.CosInner - light.CosOuter;
            attenuation = smoothstep(0.0, 1.0, pow(number / denom, light.DropOff));
        }
    }
    return attenuation;
}

float DistanceDropOff(Light light, float dist) {
    float attenuation = 0.0;
    if (dist <= light.Far) {
        if (dist <= light.Near) {
            attenuation = 1.0;
        } else {
            float n = dist - light.Near;
            float d = light.Far - light.Near;
            attenuation = smoothstep(0.0, 1.0, 1.0 - (n * n) / (d * d));
        }
    }
    return attenuation;
}

vec4 LightEffect (Light light) {
    float aAttenuation = 1.0;
    float dAttenuation = 1.0;

    if (light.LightType != eDirectionalLight) {
        vec3 lightDirection = -normalize(light.Direction.xyz);
        vec3 L = light.Position.xyz - gl_FragCoord.xyz;
        float dist = length(L);
        L = L / dist;

        if (light.LightType == eSpotlight) {
            aAttenuation = AngularDropOff(light, lightDirection, L);
        }
        dAttenuation = DistanceDropOff(light, dist);
    }
    return dAttenuation * aAttenuation * light.Intensity * light.Color;
}


void main(void) {
    vec4 textureMapColor = texture2D(uSampler, vec2(vTextureCoordinate.s, vTextureCoordinate.t));
    vec4 lightResults = uGlobalAmbientColor * uGlobalAmbientIntensity;

    if (textureMapColor.a > 0.0) {
        for (int i = 0; i < kGLSLuLightArraySize; i++) {
            if (uLights[i].IsOn) {
                lightResults += LightEffect(uLights[i]);
            }
        }
    }

    lightResults *= textureMapColor;

    // tint the texture
    vec3 color = vec3(lightResults) * (1.0 - uPixelColor.a) + vec3(uPixelColor) * uPixelColor.a;
    vec4 result = vec4(color, lightResults.a);

    gl_FragColor = result;
}
