precision mediump float;

uniform sampler2D uSampler;
uniform vec4 uPixelColor;

varying vec2 vTextureCoordinate;

#define kSufficentlyOpaque 0.1

void main(void) {
    vec4 textureFragmentColor = texture2D(uSampler, vTextureCoordinate);
    if (textureFragmentColor.a < kSufficentlyOpaque) {
        discard;
    } else {
        gl_FragColor = vec4(1, 1, 1, 1);
    }
}
