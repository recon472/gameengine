precision mediump float;

uniform sampler2D uSampler;
uniform vec4 uPixelColor;
uniform vec4 uGlobalAmbientColor;
uniform float uGlobalAmbientIntensity;
varying vec2 vTextureCoordinate;

void main(void) {
    vec4 color = texture2D(uSampler, vec2(vTextureCoordinate.s, vTextureCoordinate.t));
    color = color * uGlobalAmbientColor * uGlobalAmbientIntensity;
    vec3 tint = vec3(color) * (1.0 - uPixelColor.a) + vec3(uPixelColor) * uPixelColor.a;
    vec4 result = vec4(tint, color.a);

    gl_FragColor = result;
}
