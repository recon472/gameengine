attribute vec3 aSquareVertexPosition;
attribute vec2 aTextureCoordinate;

varying vec2 vTextureCoordinate;

uniform mat4 uModelTransform;
uniform mat4 uViewProjectionTransform;

void main(void) {
    gl_Position = uViewProjectionTransform * uModelTransform * vec4(aSquareVertexPosition, 1.0);
    vTextureCoordinate = aTextureCoordinate;
}
