precision mediump float;

// object that fethes data from texture
uniform sampler2D uSampler;
uniform sampler2D uNormalSampler;

// color information
uniform vec4 uPixelColor;
uniform vec4 uGlobalAmbientColor;
uniform float uGlobalAmbientIntensity;

// for computing V vector
uniform vec3 uCameraPosition;

// material
struct Material {
    vec4 Ka; // simple boosting of color
    vec4 Kd; // diffuse
    vec4 Ks; // specular
    float Shininess;
};
uniform Material uMaterial;

// light information
#define kGLSLuLightArraySize 4
#define ePointLight 0
#define eDirectionalLight 1
#define eSpotLight 2
struct Light {
    vec3 Position;
    vec3 Direction;
    vec4 Color;
    float Near;
    float Far;
    float CosInner;
    float CosOuter;
    float Intensity;
    float DropOff;
    bool IsOn;
    int LightType;
};
uniform Light uLights[kGLSLuLightArraySize];
varying vec2 vTextureCoordinate;

float AngularDropOff(Light light, vec3 lightDirection, vec3 L) {
    float attenuation = 0.0;
    float cosL = dot(lightDirection, L);
    float number = cosL - light.CosOuter;
    if (number > 0.0) {
        if (cosL > light.CosInner) {
            attenuation = 1.0;
        } else {
            float denom = light.CosInner - light.CosOuter;
            attenuation = smoothstep(0.0, 1.0, pow(number / denom, light.DropOff));
        }
    }
    return attenuation;
}

float DistanceDropOff(Light light, float dist) {
    float attenuation = 0.0;
    if (dist <= light.Far) {
        if (dist <= light.Near) {
            attenuation = 1.0;
        } else {
            float n = dist - light.Near;
            float d = light.Far - light.Near;
            attenuation = smoothstep(0.0, 1.0, 1.0 - (n * n) / (d * d));
        }
    }
    return attenuation;
}

vec4 SpecularResult(vec3 N, vec3 L) {
    vec3 V = normalize(uCameraPosition - gl_FragCoord.xyz);
    vec3 H = (L + V) * 0.5;
    return uMaterial.Ks * pow(max(0.0, dot(N, H)), uMaterial.Shininess);
}

vec4 DiffuseResult(vec3 N, vec3 L, vec4 textureMapColor) {
    return uMaterial.Kd * max(0.0, dot(N, L)) * textureMapColor;
}

vec4 ShadedResult(Light light, vec3 N, vec4 textureMapColor) {
    float aAttenuation = 1.0;
    float dAttenuation = 1.0;
    vec3 lightDirection = -normalize(light.Direction.xyz);
    vec3 L;
    float dist;
    if (light.LightType == eDirectionalLight) {
        L = lightDirection;
    } else {
        L = light.Position.xyz - gl_FragCoord.xyz;
        dist = length(L);
        L = L / dist;
    }
    if (light.LightType == eSpotLight) {
        aAttenuation = AngularDropOff(light, lightDirection, L);
    }
    if (light.LightType != eDirectionalLight) {
        dAttenuation = DistanceDropOff(light, dist);
    }
    vec4 diffuse = DiffuseResult(N, L, textureMapColor);
    vec4 specular = SpecularResult(N, L);
    vec4 result = aAttenuation * dAttenuation * light.Intensity * light.Color * (diffuse + specular);
    return result;
}

void main(void) {
    vec4 textureMapColor = texture2D(uSampler, vTextureCoordinate);
    vec4 normal = texture2D(uNormalSampler, vTextureCoordinate);
    vec4 normalMap = (2.0 * normal) - 1.0;
    // normalMap.y = -normalMap.y;
    vec3 N = normalize(normalMap.xyz);
    vec4 shadedResult = uMaterial.Ka + (textureMapColor * uGlobalAmbientColor * uGlobalAmbientIntensity);

    if (textureMapColor.a > 0.0) {
        for (int i = 0; i < kGLSLuLightArraySize; i++) {
            if (uLights[i].IsOn) {
                shadedResult += ShadedResult(uLights[i], N, textureMapColor);
            }
        }
    }

    vec3 tintResult = vec3(shadedResult) * (1.0 - uPixelColor.a) + vec3(uPixelColor) * uPixelColor.a;
    vec4 result = vec4(tintResult, shadedResult.a);

    gl_FragColor = result;
}
