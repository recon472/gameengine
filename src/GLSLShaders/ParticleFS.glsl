precision mediump float;

uniform sampler2D uSampler;
uniform vec4 uPixelColor;

varying vec2 vTextureCoordinate;

void main(void) {
    vec4 color = texture2D(uSampler, vec2(vTextureCoordinate.s, vTextureCoordinate.t));
    vec3 r = vec3(color) * color.a * vec3(uPixelColor);
    vec4 result = vec4(r, uPixelColor.a);

    gl_FragColor = result;
}
