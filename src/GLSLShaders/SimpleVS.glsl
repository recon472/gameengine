attribute vec3 aSquareVertexPosition;
uniform mat4 uModelTransform;
uniform mat4 uViewProjectionTransform;
uniform float uPointSize;

void main(void) {
    gl_Position = uViewProjectionTransform * uModelTransform * vec4(aSquareVertexPosition, 1.0);
    gl_PointSize = uPointSize;
}
