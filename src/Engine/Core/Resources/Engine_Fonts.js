import gEngine from "../../../GameEngine.js";

export class CharacterInfo {
    constructor () {
        this._textureCoordinateLeft = 0;
        this._textureCoordinateRight = 1;
        this._textureCoordinateBottom = 0;
        this._textureCoordinateTop = 0;

        this._characterWidth = 1;
        this._characterHeight = 1;
        this._characterWidthOffset = 0;
        this._characterHeightOffset = 0;

        this._characterAspectRatio = 1;
    }
}

let instance = null;

export default class Fonts {
    constructor () {
        if (!instance) {
            instance = this;
        }
        return instance;
    }

    loadFont (fontName) {
        if(!gEngine.ResourceMap.isAssetLoaded(fontName)) {
            let fontInfoSourceString = fontName + ".fnt";
            let textureSourceString = fontName + ".png";

            gEngine.ResourceMap.asyncLoadRequested(fontName);

            gEngine.Textures.loadTexture(textureSourceString);
            gEngine.TextFileLoader.loadTextFile(fontInfoSourceString, gEngine.TextFileLoader.eTextFileType.eXMLFile, instance._storeLoadedFont);
        } else {
            gEngine.ResourceMap.increaseAssetReferenceCount(fontName);
        }
    }

    _storeLoadedFont (fontInfoSourceString) {
        let fontName = fontInfoSourceString.slice(0, -4);
        let fontInfo = gEngine.ResourceMap.retrieveAsset(fontInfoSourceString);
        fontInfo.fontImage = fontName + ".png";
        gEngine.ResourceMap.asyncLoadCompleted(fontName, fontInfo);
    }

    unloadFont (fontName) {
        gEngine.ResourceMap.unloadAsset(fontName);
        if (!gEngine.ResourceMap.isAssetLoaded(fontName)) {
            let fontInfoSourceString = fontName + ".fnt";
            let textureSourceString = fontName + ".png";

            gEngine.Textures.unloadTexture(textureSourceString);
            gEngine.TextFileLoader.unloadTextFile(fontInfoSourceString);
        }
    }

    getCharacterInfo (fontName, character) {
        let returnInfo = null;
        let fontInfo = gEngine.ResourceMap.retrieveAsset(fontName);
        let commonPath = "font/common";
        let commonInfo = fontInfo.evaluate(commonPath, fontInfo, null, XPathResult.ANY_TYPE, null);
        commonInfo = commonInfo.iterateNext();
        if (commonInfo === null) {
            return returnInfo;
        }
        let characterHeight = commonInfo.getAttribute("base");

        let characterPath = "font/chars/char[@id=" + character + "]";
        let characterInfo = fontInfo.evaluate(characterPath, fontInfo, null, XPathResult.ANY_TYPE, null);
        characterInfo = characterInfo.iterateNext();

        if (characterInfo === null) {
            return returnInfo;
        }

        returnInfo = new CharacterInfo();
        let textureInfo = gEngine.Textures.getTextureInfo(fontInfo.fontImage);
        let leftPixel = Number(characterInfo.getAttribute("x"));
        let rightPixel = leftPixel + Number(characterInfo.getAttribute("width")) - 1;
        let topPixel = (textureInfo._height - 1) - Number(characterInfo.getAttribute("y"));
        let bottomPixel = topPixel - Number(characterInfo.getAttribute("height")) + 1;

        // texture coordinate information
        returnInfo._textureCoordinateLeft = leftPixel / (textureInfo._width - 1);
        returnInfo._textureCoordinateTop = topPixel / (textureInfo._height - 1);
        returnInfo._textureCoordinateRight = rightPixel / (textureInfo._width - 1);
        returnInfo._textureCoordinateBottom = bottomPixel / (textureInfo._height - 1);

        // relative character size
        let characterWidth = characterInfo.getAttribute("xadvance");
        returnInfo._characterWidth = characterInfo.getAttribute("width") / characterWidth;
        returnInfo._characterHeight = characterInfo.getAttribute("height") / characterHeight;
        returnInfo._characterWidthOffset = characterInfo.getAttribute("xoffset") / characterWidth;
        returnInfo._characterHeightOffset = characterInfo.getAttribute("yoffset") / characterHeight;
        returnInfo._characterAspectRatio = characterWidth / characterHeight;

        return returnInfo;
    }
}
