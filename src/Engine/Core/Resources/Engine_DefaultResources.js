import gEngine from "../../../GameEngine.js";
import {
    SimpleShader, TextureShader, SpriteShader, LightShader, IlluminationShader, LineShader, ShadowCasterShader
} from "../../../GameEngine.js";

let instance = null;

export default class DefaultResources {
    constructor () {
        // SimpleShader
        this._constantColorShader = null;
        this.kSimpleVS = "src/GLSLShaders/SimpleVS.glsl";
        this.kSimpleFS = "src/GLSLShaders/SimpleFS.glsl";

        // LighShader
        this._lightShader = null;
        this.kLightFS = "src/GLSLShaders/LightFS.glsl";

        // IlluminationShader
        this.kIlluminationFS = "src/GLSLShaders/IlluminationFS.glsl";
        this._illuminationShader = null;

        // ShadowShader
        this._shadowReceiverShader = null;
        this._shadowCasterShader = null;
        this.kShadowReceiverFS = "src/GLSLShaders/ShadowReceiverFS.glsl";
        this.kShadowCasterFS = "src/GLSLShaders/ShadowCasterFS.glsl";

        // TextureShader, SpriteShader
        this._textureShader = null;
        this._spriteShader = null;
        this.kTextureVS = "src/GLSLShaders/TextureVS.glsl";
        this.kTextureFS = "src/GLSLShaders/TextureFS.glsl";

        // line shader
        this._lineShader = null;
        this.kLineFS = "src/GLSLShaders/LineFS.glsl";

        // particle shader
        this.kParticleFS = "src/GLSLShaders/ParticleFS.glsl";
        this._particleShader = null;

        // global ambient color
        this._globalAmbientColor = [1, 1, 1, 1];
        this._globalAmbientIntensity = 1;

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    getConstantColorShader () {
        return instance._constantColorShader;
    }

    getLightShader () {
        return instance._lightShader;
    }

    getIlluminationShader () {
        return instance._illuminationShader;
    }

    getShadowReceiverShader () {
        return instance._shadowReceiverShader;
    }

    getShadowCasterShader () {
        return instance._shadowCasterShader;
    }

    getTextureShader () {
        return instance._textureShader;
    }

    getSpriteShader () {
        return instance._spriteShader;
    }

    getLineShader () {
        return instance._lineShader;
    }

    getParticleShader () {
        return instance._particleShader;
    }

    getGlobalAmbientColor () {
        return instance._globalAmbientColor;
    }

    setGlobalAmbientColor (color) {
        instance._globalAmbientColor = color;
    }

    getGlobalAmbientIntensity () {
        return instance._globalAmbientIntensity;
    }

    setGlobalAmbientIntensity (intensity) {
        instance._globalAmbientIntensity = intensity;
    }

    resetGlobalAmbient () {
        instance._globalAmbientIntensity = 1;
        instance._globalAmbientColor = [1, 1, 1, 1];
    }

    // callback after loadings are done
    _createShaders (callBackFunction) {
        instance._constantColorShader = new SimpleShader(instance.kSimpleVS, instance.kSimpleFS);
        instance._textureShader = new TextureShader(instance.kTextureVS, instance.kTextureFS);
        instance._spriteShader = new SpriteShader(instance.kTextureVS, instance.kTextureFS);
        instance._lightShader = new LightShader(instance.kTextureVS, instance.kLightFS);
        instance._illuminationShader = new IlluminationShader(instance.kTextureVS, instance.kIlluminationFS);
        instance._lineShader = new LineShader(instance.kSimpleVS, instance.kLineFS);
        instance._shadowReceiverShader = new SpriteShader(instance.kTextureVS, instance.kShadowReceiverFS);
        instance._shadowCasterShader = new ShadowCasterShader(instance.kTextureVS, instance.kShadowCasterFS);
        instance._particleShader = new TextureShader(instance.kTextureVS, instance.kParticleFS);

        callBackFunction();
    }

    // initiate asynchronous loading of GLSL Shader files
    initialize (callBackFunction) {
        // constant color Shader
        gEngine.TextFileLoader.loadTextFile(instance.kSimpleVS, gEngine.TextFileLoader.eTextFileType.eTextFileType);
        gEngine.TextFileLoader.loadTextFile(instance.kSimpleFS, gEngine.TextFileLoader.eTextFileType.eTextFileType);

        // texture Shader
        gEngine.TextFileLoader.loadTextFile(instance.kTextureVS, gEngine.TextFileLoader.eTextFileType.eTextFileType);
        gEngine.TextFileLoader.loadTextFile(instance.kTextureFS, gEngine.TextFileLoader.eTextFileType.eTextFileType);

        // light Shader
        gEngine.TextFileLoader.loadTextFile(instance.kLightFS, gEngine.TextFileLoader.eTextFileType.eTextFileType);

        // illumination shader
        gEngine.TextFileLoader.loadTextFile(instance.kIlluminationFS, gEngine.TextFileLoader.eTextFileType.eTextFileType);

        // line shader
        gEngine.TextFileLoader.loadTextFile(instance.kLineFS, gEngine.TextFileLoader.eTextFileType.eTextFileType);

        // shadow shader
        gEngine.TextFileLoader.loadTextFile(instance.kShadowCasterFS, gEngine.TextFileLoader.eTextFileType.eTextFileType);
        gEngine.TextFileLoader.loadTextFile(instance.kShadowReceiverFS, gEngine.TextFileLoader.eTextFileType.eTextFileType);

        // particle shader
        gEngine.TextFileLoader.loadTextFile(instance.kParticleFS, gEngine.TextFileLoader.eTextFileType.eTextFileType);

        // setup callback for when it is done
        gEngine.ResourceMap.setLoadCompleteCallback(() => {
            instance._createShaders(callBackFunction);
        });
    }

    cleanUp () {
        // shaders
        instance._constantColorShader.cleanUp();
        instance._textureShader.cleanUp();
        instance._spriteShader.cleanUp();
        instance._lightShader.cleanUp();
        instance._illuminationShader.cleanUp();

        // shader source files
        gEngine.TextFileLoader.unloadTextFile(instance.kSimpleVS);
        gEngine.TextFileLoader.unloadTextFile(instance.kSimpleFS);
        gEngine.TextFileLoader.unloadTextFile(instance.kTextureVS);
        gEngine.TextFileLoader.unloadTextFile(instance.kTextureFS);
        gEngine.TextFileLoader.unloadTextFile(instance.kLightFS);
        gEngine.TextFileLoader.unloadTextFile(instance.kIlluminationFS);
        gEngine.TextFileLoader.unloadTextFile(instance.kLineFS);
        gEngine.TextFileLoader.unloadTextFile(instance.kShadowCasterFS);
        gEngine.TextFileLoader.unloadTextFile(instance.kShadowReceiverFS);
        gEngine.TextFileLoader.unloadTextFile(instance.kParticleFS);
    }
}
