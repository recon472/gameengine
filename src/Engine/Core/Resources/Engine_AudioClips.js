import gEngine from "../../../GameEngine.js";

let instance = null;

export default class AudioClips {
    constructor () {
        let _audioContext = null;
        let _backgroundAudioNode = null;

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    initAudioContext () {
        try {
            let AudioContext = window.AudioContext || window.webkitAudioContext;
            instance._audioContext = new AudioContext();
        } catch (error) {
            alert("Web Audio is not supported.");
        }
    }

    loadAudio (clipName) {
        if (!gEngine.ResourceMap.isAssetLoaded(clipName)) {
            gEngine.ResourceMap.asyncLoadRequested(clipName);

            let request = new XMLHttpRequest();
            request.onreadystatechange = function () {
                if ((request.readyState === 4) && (request.status !== 200)) {
                    alert(fileName + ": loading failed! [Hint: you cannot double click index.html to run instance project. The index.html file must be loaded by a web-server.]");
                }
            };
            request.open("GET", clipName, true);
            request.responseType = "arraybuffer";
            request.onload = () => {
                instance._audioContext.decodeAudioData(request.response, (buffer) => {
                    gEngine.ResourceMap.asyncLoadCompleted(clipName, buffer);
                });
            };
            request.send();
        } else {
            gEngine.ResourceMap.increaseAssetReferenceCount(clipName);
        }
    }

    unloadAudio (clipName) {
        gEngine.ResourceMap.unloadAsset(clipName);
    }

    playCue (clipName) {
        let clipInfo = gEngine.ResourceMap.retrieveAsset(clipName);
        if (clipInfo !== null) {
            let sourceNode = instance._audioContext.createBufferSource();
            sourceNode.buffer = clipInfo;
            sourceNode.connect(instance._audioContext.destination);
            sourceNode.start(0);
        }
    }

    playBackgroundAudio (clipName) {
        let clipInfo = gEngine.ResourceMap.retrieveAsset(clipName);
        if (clipInfo !== null) {
            stopBackgroundAudio();
            instance._backgroundAudioNode = instance._audioContext.createBufferSource();
            instance._backgroundAudioNode.buffer = clipInfo;
            instance._backgroundAudioNode.connect(instance._audioContext.destination);
            instance._backgroundAudioNode.loop = true;
            instance._backgroundAudioNode.start(0);
        }
    }

    stopBackgroundAudio () {
        if (instance._backgroundAudioNode !== null) {
            instance._backgroundAudioNode.stop(0);
            instance._backgroundAudioNode = null;
        }
    }

    isBackgroundAudioPlaying () {
        return (instance._backgroundAudioNode !== null);
    }
}
