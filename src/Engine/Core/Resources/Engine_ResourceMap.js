class MapEntry {
    constructor (resourceName) {
        this._asset = resourceName;
        this._referenceCount = 1;
    }
}

let instance = null;

export default class ResourceMap {
    constructor () {
        this._resourceMap = {};
        this._numberOfOutstandingLoads = 0;
        this._loadCompleteCallback = null;

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    _checkForAllLoadCompleted () {
        if ((instance._numberOfOutstandingLoads === 0) && (instance._loadCompleteCallback !== null)) {
            let functionToCall = instance._loadCompleteCallback;
            instance._loadCompleteCallback = null;
            functionToCall();
        }
    }

    setLoadCompleteCallback (func) {
        instance._loadCompleteCallback = func;
        instance._checkForAllLoadCompleted();
    }

    asyncLoadRequested (resourceName) {
        instance._resourceMap[resourceName] = new MapEntry(resourceName);
        instance._numberOfOutstandingLoads++;
    }

    asyncLoadCompleted (resourceName, loadedAsset) {
        if (!instance.isAssetLoaded(resourceName)) {
            alert("gEngine.asyncLoadCompleted: [" + rName + "] not in map!");
        }
        instance._resourceMap[resourceName]._asset = loadedAsset;
        instance._numberOfOutstandingLoads--;
        instance._checkForAllLoadCompleted();
    }

    isAssetLoaded (resourceName) {
        return (resourceName in instance._resourceMap);
    }

    retrieveAsset (resourceName) {
        let resource = null;
        if (resourceName in instance._resourceMap) {
            resource = instance._resourceMap[resourceName]._asset;
        }
        return resource;
    }

    increaseAssetReferenceCount (resourceName) {
        instance._resourceMap[resourceName]._referenceCount += 1;
    }

    getAssetReferenceCount (resourceName) {
        return instance._resourceMap[resourceName]._referenceCount;
    }

    unloadAsset (resourceName) {
        let count = 0;
        if (resourceName in instance._resourceMap) {
            instance._resourceMap[resourceName]._referenceCount -= 1;
            count = instance._resourceMap[resourceName]._referenceCount;
            if (count === 0) {
                delete instance._resourceMap[resourceName];
            }
        }
        return count;
    }
}
