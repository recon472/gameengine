import gEngine from "../../../GameEngine.js";

let instance = null;

export default class ProceduralTexture {
    constructor () {
        this._canvas = null;
        this._context = null;

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    initialize () {
        instance._canvas = document.createElement('canvas');
        instance._canvas.style = "display:none";
        instance._canvas.id = "proceduralCanvas";
        const body = document.getElementsByTagName("body")[0];
        body.appendChild(instance._canvas);
        instance._context = instance._canvas.getContext("2d");
    }

    cleanUp () {
        const body = document.getElementsByTagName("body")[0];
        body.removeChild(instance._canvas);
        instance._canvas = null;
        instance._context = null;
    }

    getPowerOfTwo (value, power) {
        power = power || 1;
        while (power < value) {
            power *= 2;
        }
        return power;
    }

    textureFromCanvas (callback) {
        const url = instance._canvas.toDataURL();
        let image = new Image();
        image.onload = () => {
            const gl = gEngine.Core.getGL();
            let texture = gl.createTexture();

            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
            gl.generateMipmap(gl.TEXTURE_2D);
            gl.bindTexture(gl.TEXTURE_2D, null);

            callback(texture);
        }
        image.src = url;
    }

    fromArray (array, width, height, callback) {
        let gl = gEngine.Core.getGL();
        let data = array instanceof Uint8Array ? array : new Uint8Array(array);
        let texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, data);
        gl.generateMipmap(gl.TEXTURE_2D);
        gl.bindTexture(gl.TEXTURE_2D, null);

        callback(texture, width, height);
    }

    fromText (text, color, size, font, alignment, callback) {
        let setParameters = () => {
            instance._context.fillStyle = color;
            instance._context.textAlign = horizontalAlignment;
            instance._context.baseline = verticalAlignment;
            instance._context.font = size + "px " + font;
        };
        
        let horizontalAlignment = "center";
        let verticalAlignment = "middle";

        setParameters();

        let textWidth = instance._context.measureText(text).width;
        let width = instance.getPowerOfTwo(textWidth);
        let height = instance.getPowerOfTwo(2 * size);

        instance._canvas.width = width;
        instance._canvas.height = height;

        setParameters();

        switch (alignment) {
            case "left":
                instance._context.fillText(text, textWidth / 2, instance._canvas.height / 2);
                break;
            case "center":
                instance._context.fillText(text, instance._canvas.width / 2, instance._canvas.height / 2);
                break;
            case "right":
                instance._context.fillText(text, instance._canvas.width - textWidth / 2, instance._canvas.height / 2);
                break;
        }

        instance.textureFromCanvas((texture) => {
            callback(texture, width, height);
        });
    }

    fromNoiseMap (noiseMap, callback) {
        let array = noiseMap.getArray();
        let data = new Uint8Array(array.length * 4);
        for (let i = 0; i < array.length; i++) {
            data[i * 4] = array[i] * 255;
            data[i * 4 + 1] = array[i] * 255;
            data[i * 4 + 2] = array[i] * 255;
            data[i * 4 + 3] = 255;
        }

        instance.fromArray(data, noiseMap.getWidth(), noiseMap.getHeight(), (texture, width, height) => {
            callback(texture, width, height);
        });
    }
}
