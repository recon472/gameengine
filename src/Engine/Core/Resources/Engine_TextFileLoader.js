import gEngine from "../../../GameEngine.js";

let instance = null;

export default class TextFileLoader {
    constructor () {
        this.eTextFileType = Object.freeze({
            eXMLFile: 0,
            eTextFile: 1
        });

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    loadTextFile (fileName, fileType, callbackFunction) {
        // check if it already isn't loaded, if it is, just call the callback function with the asset
        if (!gEngine.ResourceMap.isAssetLoaded(fileName)) {
            // update resources in load counter
            gEngine.ResourceMap.asyncLoadRequested(fileName);

            // async request data from server
            var request = new XMLHttpRequest();
            request.onreadystatechange = function () {
                if ((request.readyState === 4) && (request.status !== 200)) {
                    alert(fileName + ": loading failed! [Hint: you cannot double click index.html to run this project. The index.html file must be loaded by a web-server.]");
                }
            };
            request.open("GET", fileName, true);
            request.setRequestHeader("Content-Type", "text/xml");
            request.onload = () => {
                var fileContent = null;
                if (fileType === instance.eTextFileType.eXMLFile) {
                    var parser = new DOMParser();
                    fileContent = parser.parseFromString(request.responseText, "text/xml");
                } else {
                    fileContent = request.responseText;
                }
                // let the resouremap know that the load is complete and store the asset
                gEngine.ResourceMap.asyncLoadCompleted(fileName, fileContent);

                if ((callbackFunction !== null) && (callbackFunction !== undefined)) {
                    callbackFunction(fileName);
                }
            };
            request.send();
        } else {
            if ((callbackFunction !== null) && (callbackFunction !== undefined)) {
                callbackFunction(fileName);
            }
        }
    }

    unloadTextFile (fileName) {
        gEngine.ResourceMap.unloadAsset(fileName);
    }
}
