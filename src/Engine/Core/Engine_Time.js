let instance = null;

export default class Time {
    constructor () {
        this._startTime = 0;
        this._gameSpeed = 1;

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    initialize () {
        instance._startTime = Date.now();
    }

    getElapsedTime () {
        return Date.now() - instance._startTime;
    }

    getGameSpeed () {
        return instance._gameSpeed;
    }

    setGameSpeed (speed) {
        instance._gameSpeed = Math.max(speed, 0);
    }

    increaseGameSpeedBy (delta) {
        instance._gameSpeed = Math.max(instance._gameSpeed + delta, 0);
    }
}
