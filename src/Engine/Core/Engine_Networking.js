import gEngine from "../../GameEngine.js";
import { NetworkPacket } from "../../GameEngine.js";

let instance = null;

export default class Networking {
    constructor () {
        this._master = false;
        this._clients = [];
        this._timer = null;
        this._currentScene = null;
        this._isSceneLoading = false;
        this._socket = null;
        this._updateInterval = 15 // 15x per second
        this._keepUserObjectsOnDisconnect = false;
        this._data = null;
        this._privatePackets = [];
        this._cachedPackets = [];
        this._objectPool = {};
        this._objectIDPool = 0;
        this._removeOwnedObjectsOnDisconnect = true;
        this._loadSceneOnDemand = true;
        this._loadOnlyDifferentScenes = true;

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    setKeepUserObjectsOnDisconnect (value) {
        instance._keepUserObjectsOnDisconnect = value;
    }

    getKeepUserObjectsOnDisconnect () {
        return instance._keepUserObjectsOnDisconnect;
    }

    setRemoveOwnedObjectsOnDisconnect (value) {
        instance._removeOwnedObjectsOnDisconnect = value;
    }

    getRemoveOwnedObjectsOnDisconnect () {
        return instance._removeOwnedObjectsOnDisconnect;
    }

    getHTTPURL () {
        return window.location.href;
    }

    getClientID () {
        return instance._socket.id || null;
    }

    isMasterClient () {
        return this._master;
    }

    isConnected () {
        return instance._socket.connected || false;
    }

    connectToMasterServer (url) {
        if (instance._socket !== null) {
            instance._socket.close();
        }
        instance._socket = io(url, { "transports" : ["websocket"] });
    }

    createRoomIfNotExistAndJoin (name, map, mode, maxPlayers, callback = ()=>{}) {
        instance._socket.emit("createInstance", {
            name: name,
            map: map,
            mode: mode,
            maxPlayers: maxPlayers
        }, () => {
            instance.joinRoom(name, (success) => {
                callback(success);
            });
        });
    }

    joinRoom (name, callback = ()=>{}) {
        instance._socket.emit("joinInstance", { name: name }, (port, error) => {
            console.log(`Joined room named: ${name}, port: ${port}, error: ${error}`);
            if (error !== null) {
                callback(false);
                return;
            }
            const element = document.createElement("a");
            element.href = instance._socket.io.uri;
            element.port = port;
            instance.connectToServer(element.href, () => {
                callback(true);
            });
        });
    }

    connectToServer (url, callback = ()=>{}) {
        instance.connectToMasterServer(url);
        instance._socket.on("welcome", (data) => {
            instance._data = new NetworkPacket();
            instance._clients = data.clients;
            console.log(`MP - Connected to ${url}`);
            console.log(`MP - Client ID: ${instance._socket.id}`);
            console.log(`MP - Server Message: ${data.text}`);
            callback(true);
        });
        instance._socket.on("master", () => {
            instance._master = true;
            console.log("master");
        });
        instance._socket.on("packet", (data) => {
            instance._processNetworkBatch(data.data);
        });
        instance._socket.on("userDisconnected", (data) => {
            if (!instance._keepUserObjectsOnDisconnect) {
                instance.deleteObjectsOwnedBy(data.id);
            }
            gEngine.GameLoop.getCurrentScene().userDisconnected(data.id);
        });
        instance._socket.on("userConnected", (data) => {
            let packet = new NetworkPacket();

            instance._clients.push(data.id);

            // load the same scene if we want and we have loaded one
            if (instance._currentScene !== null && instance._loadSceneOnDemand) {
                packet.load = instance._currentScene;
            }

            // create objects for the new client that I own
            for (let objectID in instance._objectPool) {
                const gameObject = instance._objectPool[objectID];
                if (gameObject.isOwner()) {
                    instance._pushCreateToPacket(packet, gameObject.constructor.name, gameObject.getOwner(), gameObject.getNetworkID());
                }
            }

            instance._sendPrivatePacket(packet, [data.id]);

            gEngine.GameLoop.getCurrentScene().userConnected(data.id);
        });
        instance._socket.on('pong', (latency) => {
            console.log("Server latency: "+ latency + "ms");
        });
    }

    disconnect () {
        instance._socket.close();
    }

    deleteObjectsOwnedBy (id) {
        const index = instance._clients.indexOf(id);
        if (index !== -1) {
            instance._clients.splice(index, 1);
        }

        if (instance._removeOwnedObjectsOnDisconnect) {
            const userID = id;
            for (let objectID in instance._objectPool) {
                const gameObject = instance._objectPool[objectID];
                if (gameObject.getOwner() === userID) {
                    gameObject.destroy();
                }
            }
        }
    }

    loadSceneGlobal (scene) {
        instance._data.load = scene;
        instance.loadSceneLocal(scene);
    }

    loadSceneLocal (scene) {
        instance._isSceneLoading = true;
        gEngine.GameLoop.stop(() => {
            instance._currentScene = scene;
            instance._objectPool = [];
            const level = eval("new gEngine.SceneManifest." + scene + "()");
            gEngine.Core.startScene(level);
            console.log("MP - Level Loaded: " + scene);
            instance._isSceneLoading = false;
            instance._processCachedPackets();
        });
    }

    addGameObject (gameObject) {
        const objectName = gameObject.constructor.name;
        const owner = instance.getClientID();
        const networkID = instance._drawNewObjectID();

        instance._pushCreateToPacket(instance._data, objectName, owner, networkID);
        instance._addGameObject(gameObject, owner, networkID);
    }

    _pushCreateToPacket (packet, objectName, objectOwner, objectNetworkID) {
        packet.create.push({
            name: objectName,
            owner: objectOwner,
            networkID: objectNetworkID
        });
    }

    transferObjectOwnership (objectNetworkID, newOwner) {
        instance._data.ownership[objectNetworkID] = newOwner;
        instance._setOwnership(objectNetworkID, newOwner);
    }

    _setOwnership(objectNetworkID, newOwner) {
        const gameObject = instance._objectPool[objectNetworkID];
        gameObject._owner = newOwner;
    }

    deleteGameObject (gameObject) {
        if (instance._objectPool[gameObject._networkID] !== undefined && instance._objectPool[gameObject._networkID] !== null) {
            if (gameObject.isOwner()) {
                instance._data.remove.push(gameObject._networkID);
            }
            delete(instance._objectPool[gameObject._networkID]);
        }
    }

    _addGameObject (gameObject, owner, networkID) {
        gameObject._owner = owner;
        gameObject._networkID = networkID;
        instance._objectPool[networkID] = gameObject;
        const scene = gEngine.GameLoop.getCurrentScene();
        if (scene !== null) {
            scene.getGameWorld().addGameObject(gameObject);
        }
    }

    _drawNewObjectID () {
        return instance.getClientID() + instance._objectIDPool++;
    }

    _extractObservedValuesFromNetworkObject (objects, updateObject) {
        for (let objectName in objects) {
            const object = objects[objectName];
            if (object._observedValues.length > 0 || object._triggers.length > 0) {
                const data = {};
                for (let value of object._observedValues) {
                    data[value.name] = object[value.name];
                }
                if (object._triggers.length > 0) {
                    data["_triggers"] = object._triggers;
                    object._triggers = [];
                }
                updateObject[objectName] = data;
            }
        }
    }

    _sendNetworkBatch () {
        if (!instance.isConnected() || instance._data === null) {
            return;
        }

        // add values from gameobjects
        for (let objectID in instance._objectPool) {
            const gameObject = instance._objectPool[objectID];
            if (gameObject.isOwner()) {
                if (!gameObject.isSendingUpdatePackets()) {
                    continue;
                }

                let updateObject = {};
                instance._extractObservedValuesFromNetworkObject({gameObject:gameObject}, updateObject);
                instance._extractObservedValuesFromNetworkObject(gameObject._componentPool, updateObject);
                instance._data.update[gameObject.getNetworkID()] = updateObject;
            }
        }

        instance._socket.emit("packet", { data: instance._data } );
        instance._data = new NetworkPacket();
        instance._socket.emit("privatePacket", { data: instance._privatePackets } );
        instance._privatePackets = [];
    }

    _sendPrivatePacket (packet, playerIDs = []) {
        if (playerIDs.length <= 0) {
            playerIDs = instance._clients;
        }
        instance._privatePackets.push({
            data: packet,
            ids: playerIDs
        });
    }

    _processCachedPackets () {
        for (let packet of instance._cachedPackets) {
            instance._processNetworkBatch(packet);
        }
        instance._cachedPackets = [];
    }

    _processNetworkBatch (data) {
        // load scenes if requested
        if (data.load !== null && data.load !== undefined) {
            if (!instance._loadOnlyDifferentScenes || data.load !== instance._currentScene) {
                instance.loadSceneLocal(data.load);
            }
        }

        if (instance._isSceneLoading) {
            instance._cachedPackets.push(data);
            return;
        }

        // create new objects
        for (let object of data.create) {
            const gameObject = eval("new gEngine.GameObjectManifest." + object.name + "()");
            instance._addGameObject(gameObject, object.owner, object.networkID);
        }

        // update objects values
        for (let objectID in data.update) {
            const gameObject = instance._objectPool[objectID] || null;
            if (gameObject !== null) {
                const updateObject = data.update[objectID];
                for (let componentName in updateObject) {
                    const component = updateObject[componentName];
                    for (let key in component) {
                        const value = component[key];
                        let object;
                        if (componentName === "gameObject") {
                            object = gameObject;
                        } else {
                            object = gameObject._componentPool[componentName];
                        }
                        if (key === "_triggers") {
                            const triggers = [];
                            for (let trigger of value) {
                                if (trigger.recipients.length > 0) {
                                    for (let recipient of trigger.recipients) {
                                        if (recipient === instance.getClientID()) {
                                            triggers.push(trigger);
                                            break;
                                        }
                                    }
                                } else {
                                    triggers.push(trigger);
                                }
                            }
                            object._evaluateRemoteTriggers(triggers);
                        } else {
                            object._valuesCache[key] = value;
                        }
                    }
                }
            }
        }

        // set new ownership
        for (let objectID in data.ownership) {
            instance._setOwnership(objectID, data.ownership[objectID]);
        }

        // delete objects
        for (let networkID of data.remove) {
            if (instance._objectPool[networkID] !== null && instance._objectPool[networkID] !== undefined) {
                instance._objectPool[networkID].destroy();
                delete(instance._objectPool[networkID]);
            }
        }
    }

    resetNetworkScene () {
        instance._currentScene = null;
    }

    isSendingPackets () {
        return (instance._timer !== null);
    }

    startSendingPackets () {
        if (instance.isSendingPackets()) {
            instance.stopSendingPackets();
        }
        instance._timer = setInterval(() => {
            instance._sendNetworkBatch();
        }, 1000 / instance._updateInterval);
    }

    stopSendingPackets () {
        clearInterval(instance._timer);
        instance._timer = null;
    }

    getPlayerIDs () {
        return instance._clients;
    }

    sendSinglePacket () {
        instance._sendNetworkBatch();
    }

    setNetworkUpdateRatePerSecond (rate) {
        instance._updateInterval = rate;
        if (instance.isSendingPackets) {
            instance.startSendingPackets();
        }
    }

    getNetworkUpdateRatePerSecond () {
        return this._updateInterval;
    }
}
