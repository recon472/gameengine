import { CollisionInfo } from "../../GameEngine.js";

let instance = null;

export default class Physics {
    constructor () {
        this._relaxationCount = 15;
        this._relaxationOffset = 1 / this._relaxationCount;
        this._positionCorrectionRate = 0.8;
        this._systemAcceleration = [0, -50];

        this._relaxationLoopCount = 0;
        this._hasOneCollision = false;

        this._collisionInfo = null;

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    initialize () {
        instance._collisionInfo = new CollisionInfo();
    }

    _positionCorrection (shape1, shape2, collisionInfo) {
        let shape1InversedMass = shape1.getInversedMass();
        let shape2InversedMass = shape2.getInversedMass();
        let number = collisionInfo.getDepth() / (shape1InversedMass + shape2InversedMass) * instance._positionCorrectionRate;
        let correctionAmount = [0, 0];
        vec2.scale(correctionAmount, collisionInfo.getNormal(), number);

        let ca = [0, 0];
        vec2.scale(ca, correctionAmount, shape1InversedMass);
        let shape1Position = shape1.getPosition();
        vec2.subtract(shape1Position, shape1Position, ca);

        vec2.scale(ca, correctionAmount, shape2InversedMass);
        let shape2Position = shape2.getPosition();
        vec2.add(shape2Position, shape2Position, ca);
    }

    _applyFriction (normal, velocity, friction, mass) {
        let tangent = vec2.fromValues(normal[1], -normal[0]);
        let tangentComponent = vec2.dot(velocity, tangent);
        if (Math.abs(tangentComponent) < 0.01) {
            return;
        }
        friction *= mass * instance._relaxationOffset;
        if (tangentComponent < 0) {
            vec2.scale(tangent, tangent, -friction);
        } else {
            vec2.scale(tangent, tangent, friction);
        }
        vec2.sub(velocity, velocity, tangent);
    }

    resolveCollision (shape1, shape2, collisionInfo) {
        // one collision has been found
        instance._hasOneCollision = true;

        // correct positions
        instance._positionCorrection(shape1, shape2, collisionInfo);

        // apply friction
        let shape1Velocity = shape1.getVelocity();
        let shape2Velocity = shape2.getVelocity();
        let normal = collisionInfo.getNormal();
        instance._applyFriction(normal, shape1Velocity, shape1.getFriction(), shape1.getInversedMass());
        instance._applyFriction(normal, shape2Velocity, -shape2.getFriction(), shape2.getInversedMass());

        // compute relative velocity
        let relativeVelocity = [0, 0];
        vec2.sub(relativeVelocity, shape2Velocity, shape1Velocity);

        // check normal component direction
        let relativeVelocityInNormal = vec2.dot(relativeVelocity, normal);
        if (relativeVelocityInNormal > 0) {
            return;
        }

        // compute and apply response
        let newRestitution = Math.min(shape1.getRestitution(), shape2.getRestitution());
        let j = -(1 + newRestitution) * relativeVelocityInNormal;
        j = j / (shape1.getInversedMass() + shape2.getInversedMass());

        let impulse = [0, 0];
        vec2.scale(impulse, collisionInfo.getNormal(), j);

        let newImpulse = [0, 0];
        vec2.scale(newImpulse, impulse, shape1.getInversedMass());
        vec2.sub(shape1Velocity, shape1Velocity, newImpulse);

        vec2.scale(newImpulse, impulse, shape2.getInversedMass());
        vec2.add(shape2Velocity, shape2Velocity, newImpulse);
    }

    beginRelaxation () {
        instance._relaxationLoopCount = instance._relaxationCount;
        instance._hasOneCollision = true;
    }

    continueRelaxation () {
        let oneCollision = instance._hasOneCollision;
        instance._hasOneCollision = false;
        instance._relaxationLoopCount = instance._relaxationLoopCount - 1;
        return ((instance._relaxationLoopCount > 0) && oneCollision);
    }

    processObjectObject (object1, object2) {
        let shape1 = object1.getPhysics();
        let shape2 = object2.getPhysics();
        if (shape1 === shape2) {
            return;
        }
        instance.beginRelaxation();
        while (instance.continueRelaxation()) {
            if (shape1.collided(shape2, instance._collisionInfo)) {
                instance.resolveCollision(shape1, shape2, instance._collisionInfo);
            }
        }
    }

    processObjectSet (object, set) {
        let shape1 = object.getPhysics();
        let i, shape2;
        instance.beginRelaxation();
        while (instance.continueRelaxation()) {
            for (i = 0; i < set.size(); i++) {
                shape2 = set.getObjectAt(i).getPhysics();
                if ((shape1 !== shape2) && (shape1.collided(shape2, instance._collisionInfo))) {
                    instance.resolveCollision(shape1, shape2, instance._collisionInfo);
                }
            }
        }
    }

    processSetSet (set1, set2) {
        let i, j, shape1, shape2;
        instance.beginRelaxation();
        while (instance.continueRelaxation()) {
            for (i = 0; i < set1.size(); i++) {
                shape1 = set1.getObjectAt(i).getPhysics();
                for (j = 0; j < set2.size(); j++) {
                    shape2 = set2.getObjectAt(j).getPhysics();
                    if ((shape1 !== shape2) && (shape1.collided(shape2, instance._collisionInfo))) {
                        instance.resolveCollision(shape1, shape2, instance._collisionInfo);
                    }
                }
            }
        }
    }

    getSystemAcceleration () {
        return instance._systemAcceleration;
    }

    setSystemAcceleration (acceleration) {
        instance._systemAcceleration = acceleration;
    }

    getRelaxationCorrectionRate () {
        return instance._positionCorrectionRate;
    }

    setRelaxationCorrectionRate (rate) {
        if ((rate <= 0) && (rate >= 1)) {
            rate = 0.8;
        }
        instance._positionCorrectionRate = rate;
    }

    getRelaxationLoopCount () {
        return instance._relaxationLoopCount;
    }

    setRelaxationLoopCount (count) {
        if (count <= 0) {
            count = 1;
        }
        instance._relaxationCount = count;
        instance._relaxationOffset = 1 / instance._relaxationCount;
    }
}
