import gEngine from "../../GameEngine.js";

let instance = null;

export default class VertexBuffer {
    constructor () {
        // vertices for the square
        this.verticesOfSquare = [
            0.5, 0.5, 0.0,
            -0.5, 0.5, 0.0,
            0.5, -0.5, 0.0,
            -0.5, -0.5, 0.0
        ];

        this.verticesOfLine = [
            0.5, 0.5, 0,
            -0.5, -0.5, 0
        ];

        this.textureCoordinates = [
            1.0, 1.0,
            0.0, 1.0,
            1.0, 0.0,
            0.0, 0.0
        ];

        // reference to the vertex positions in gl context
        this._squareVertexBuffer = null;
        this._textureCoordinatesBuffer = null;
        this._lineVertexBuffer = null;

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    getGLVertexReference () {
        return instance._squareVertexBuffer;
    }

    getGLTextureCoordinatesReference () {
        return instance._textureCoordinatesBuffer;
    }

    getGLLineVertexReference () {
        return instance._lineVertexBuffer;
    }

    initialize () {
        const gl = gEngine.Core.getGL();

        // square buffer
        instance._squareVertexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, instance._squareVertexBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(instance.verticesOfSquare), gl.STATIC_DRAW);

        // texture buffer
        instance._textureCoordinatesBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, instance._textureCoordinatesBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(instance.textureCoordinates), gl.STATIC_DRAW);

        // line buffer
        instance._lineVertexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, instance._lineVertexBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(instance.verticesOfLine), gl.STATIC_DRAW);
    }

    cleanUp () {
        const gl = gEngine.Core.getGL();
        gl.deleteBuffer(instance._squareVertexBuffer);
        gl.deleteBuffer(instance._textureCoordinatesBuffer);
        gl.deleteBuffer(instance._lineVertexBuffer);
    }
}
