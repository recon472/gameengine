import gEngine from "../../GameEngine.js";
import { GameWorld } from "../../GameEngine.js";

let instance = null;

export default class GameLoop {
    constructor () {
        this.kFPS = 60;
        this.kFrameTime = 1 / this.kFPS;
        this.kMPF = 1000 * this.kFrameTime; // milliseconds per frame

        // letiables for timing the GameLoop
        this._previousTime;
        this._lagTime;
        this._currentTime;
        this._elapsedTime;

        // the current loop _constantColorShader
        this._isLoopRunning = false;

        // reference to game logic
        this._myGame = null;

        this._stopCallback = null;


        if (!instance) {
            instance = this;
        }
        return instance;
    }

    // This function assumes it is sub-classed from MyGame
    _runLoop () {
        if (instance._isLoopRunning) {
            // set up for next call to _runLoop and update input
            requestAnimationFrame( () => { instance._runLoop.call(instance._myGame) } );

            // compute elapsed time since last loop
            instance._currentTime = Date.now();
            instance._elapsedTime = instance._currentTime - instance._previousTime;
            instance._previousTime = instance._currentTime;
            instance._lagTime += instance._elapsedTime;

            // update, dropframes if needed
            while ((instance._lagTime >= instance.kMPF) && instance._isLoopRunning) {
                gEngine.Input.update();
                this.update(); // call MyGame.update()
                this._gameWorld.update();
                instance._lagTime -= instance.kMPF;
            }

            // draw
            this._gameWorld.draw(); // call MyGame.draw()
        } else {
            instance._myGame.unloadScene();
            if (instance._stopCallback !== null) {
                instance._stopCallback();
            }
        }
    }

    _startLoop() {
        instance._previousTime = Date.now();
        instance._lagTime = 0.0;

        instance._isLoopRunning = true;

        requestAnimationFrame( () => { instance._runLoop.call(instance._myGame); } );
    }

    start (myGame) {
        instance._myGame = myGame;
        gEngine.ResourceMap.setLoadCompleteCallback(
            () => {
                instance._myGame._gameWorld = new GameWorld();
                instance._myGame.initialize();
                instance._startLoop();
            }
        );
    }

    stop (callback = null) {
        instance._isLoopRunning = false;
        instance._stopCallback = callback;
    }

    getUpdateIntervalInSeconds () {
        return instance.kFrameTime;
    }

    getDrawTime () {
        return this._elapsedTime;
    }

    getFPS () {
        return Math.floor(1000 / this.getDrawTime());
    }

    getCurrentScene () {
        return instance._myGame;
    }
}
