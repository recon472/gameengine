let instance = null;

export default class Particles {
    constructor () {
        this._systemAcceleration = [0, -50];
        this._vectorFrom1to2 = [0, 0];
        this._vector = [0, 0];
        this._normal = [0, 0];

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    resolveCircleParticle (circle, particle) {
        let collided = false;
        let particlePosition = particle.getPosition();
        let circlePosition = circle.getPosition();
        vec2.subtract(instance._vectorFrom1to2, particlePosition, circlePosition);
        let distance = vec2.length(instance._vectorFrom1to2);
        if (distance < circle.getRadius()) {
            vec2.scale(instance._vectorFrom1to2, instance._vectorFrom1to2, 1 / distance);
            vec2.scaleAndAdd(particlePosition, circlePosition, instance._vectorFrom1to2, circle.getRadius());
            collided = true;
        }
        return collided;
    }

    resolveRectangleParticle (rectangle, particle) {
        let collided = false;
        let alongX = rectangle.getWidth() / 2;
        let alongY = rectangle.getHeight() / 2;

        let particlePosition = particle.getPosition();
        let rectanglePosition = rectangle.getPosition();

        let rectangleMinX = rectanglePosition[0] - alongX;
        let rectangleMaxX = rectanglePosition[0] + alongX;
        let rectangleMinY = rectanglePosition[1] - alongY;
        let rectangleMaxY = rectanglePosition[1] + alongY;

        collided = ((rectangleMinX < particlePosition[0]) && (rectangleMinY < particlePosition[1]) &&
                    (rectangleMaxX > particlePosition[0]) && (rectangleMaxY > particlePosition[1]));

        if (collided) {
            vec2.subtract(instance._vectorFrom1to2, particlePosition, rectanglePosition);
            instance._vector[0] = instance._vectorFrom1to2[0];
            instance._vector[1] = instance._vectorFrom1to2[1];

            if (Math.abs(instance._vectorFrom1to2[0] - alongX) < Math.abs(instance._vectorFrom1to2[1] - alongY)) {
                instance._normal[0] = 0;
                instance._normal[1] = 1;
                if (instance._vector[0] > 0) {
                    instance._vector[0] = alongX;
                } else {
                    instance._vector[0] = -alongX;
                }
            } else {
                instance._normal[0] = 1;
                instance._normal[1] = 0;
                if (instance._vector[1] > 0) {
                    instance._vector[1] = alongY;
                } else {
                    instance._vector[1] = -alongY;
                }
            }

            vec2.subtract(instance._vector, instance._vector, instance._vectorFrom1to2);
            vec2.add(particlePosition, particlePosition, instance._vector);
        }
        return collided;
    }

    processObjectEmitter (object, emitter) {
        let shape = object.getPhysics();
        let i, particle;
        for (i = 0; i < emitter.getParticleCount(); i++) {
            particle = emitter.getParticleAt(i).getPhysics();
            shape.resolveParticleCollision(particle);
        }
    }

    processSetSet (objectSet, particleSet) {
        for (let i = 0; i < objectSet.size(); i++) {
            instance.processObjectEmitter(objectSet.getObjectAt(i), particleSet);
        }
    }

    getSystemAcceleration () {
        return this._systemAcceleration;
    }

    setSystemAcceleration (acceleration) {
        this._systemAcceleration = acceleration;
    }
}
