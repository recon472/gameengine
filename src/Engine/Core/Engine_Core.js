import gEngine from "../../GameEngine.js";

let instance = null;

export default class Core {
    constructor () {
        this._gL = null;
        this._canvas = null;
        this._idPool = 0;

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    // getter of context
    getGL () {
        return instance._gL;
    }

    setResolution (width, height) {
        instance._canvas.width = width;
        instance._canvas.height = height;
    }

    setFullscreen () {
        instance._canvas.width = window.innerWidth;
        instance._canvas.height = window.innerHeight;
    }

    setCanvasSize (width, height) {
        instance._canvas.width = width;
        instance._canvas.height = height;
    }

    getCanvas () {
        return instance._canvas;
    }


    getNewId () {
        const id = instance._idPool;
        instance._idPool += 1;
        return id;
    }

    // initialize webgl
    _initializeWebGL (htmlCanvasID) {
        instance._canvas = document.getElementById(htmlCanvasID);

        // bind webgl to _gL and Canvas (opaque - alpha:false)
        instance._gL = instance._canvas.getContext("webgl", {alpha: false, depth: true, stencil: true}) || instance._canvas.getContext("experimental-webgl", {alpha: false, depth: true, stencil: true});

        // allow transparency with textures when the alpha channel is present
        instance._gL.blendFunc(instance._gL.SRC_ALPHA, instance._gL.ONE_MINUS_SRC_ALPHA);
        instance._gL.enable(instance._gL.BLEND);

        // flip image y axis - set origin to left-bottom corner
        instance._gL.pixelStorei(instance._gL.UNPACK_FLIP_Y_WEBGL, true);

        // make sure depth testing is enabled
        instance._gL.enable(instance._gL.DEPTH_TEST);
        instance._gL.depthFunc(instance._gL.LEQUAL);

        // check if the browser supports webGL
        if (instance._gL === null) {
            document.write("<br><b>WebGL is not supported!</b>");
            return;
        }

        // disable right click context menu
        instance._canvas.oncontextmenu = function (event) {
            return false;
        };

        // disable scrolling
        const common = "margin:0px; padding:0px; height:100%; width:100%;";
        const body = document.getElementsByTagName("body")[0];
        body.style = common + " position: fixed; overflow: hidden;";
        const html = document.getElementsByTagName("html")[0];
        html.style = common;

        // update scene on window resize
        window.addEventListener("resize", (event) => {
            let scene = gEngine.GameLoop.getCurrentScene();
            if (scene !== null) {
                scene.windowResized();
            }
        });
    }

    // initialize EngineCore
    initializeEngineCore (htmlCanvasID, scene) {
        // initialize webGL
        instance._initializeWebGL(htmlCanvasID);

        // initialize Engine Components
        gEngine.VertexBuffer.initialize();
        gEngine.Input.initialize(htmlCanvasID);
        gEngine.AudioClips.initAudioContext();
        gEngine.ProceduralTexture.initialize();
        gEngine.Time.initialize();
        gEngine.Physics.initialize();

        // initialize Default Resources, when done, invoke startScene
        gEngine.DefaultResources.initialize(() => {
            instance.startScene(scene);
        });
    }

    startScene (scene) {
        Math.seedrandom(Date.now());
        gEngine.DefaultResources.resetGlobalAmbient();
        scene.loadScene.call(scene);
        gEngine.GameLoop.start(scene);
    }

    // clears the draw area and draws one square over it
    clearCanvas (color) {
        instance._gL.clearColor(color[0], color[1], color[2], color[3]);
        instance._gL.clear(instance._gL.COLOR_BUFFER_BIT | instance._gL.STENCIL_BUFFER_BIT | instance._gL.DEPTH_BUFFER_BIT);
    }

    cleanUp () {
        gEngine.VertexBuffer.cleanUp();
        gEngine.DefaultResources.cleanUp();
        gEngine.ProceduralTexture.cleanUp();

        gEngine.TextTextureGenerator.cleanUp();
    }
}
