import gEngine from "../../GameEngine.js";

export class TextureInfo {
    constructor (name, width, height, id) {
        this._name = name;
        this._width = width;
        this._height = height;
        this._gLTextureId = id;
        this._colorArray = null;
    }
}

let instance = null;

export default class Textures {
    construtor () {
        if (!instance) {
            instance = this;
        }
        return instance;
    }

    loadTexture (textureName) {
        if (!gEngine.ResourceMap.isAssetLoaded(textureName)) {
            let image = new Image();

            gEngine.ResourceMap.asyncLoadRequested(textureName);

            // when the texture loads, convert to webGL format and store it
            image.onload = () => {
                this._processLoadedImage(textureName, image);
            };
            image.src = textureName;
        } else {
            gEngine.ResourceMap.increaseAssetReferenceCount(textureName);
        }
    }

    unloadTexture (textureName) {
        const gl = gEngine.Core.getGL();
        const textureInfo = gEngine.ResourceMap.retrieveAsset(textureName);
        gl.deleteTexture(textureInfo._gLTextureId);
        gEngine.ResourceMap.unloadAsset(textureName);
    }

    _processLoadedImage (textureName, image) {
        const gl = gEngine.Core.getGL();

        // create webGL textureInfo
        let textureId = gl.createTexture();

        // bind texture with webGL texture functionality
        gl.bindTexture(gl.TEXTURE_2D, textureId);

        // Load the texture into the texture data structure with descriptive info.
        // Parameters:
        //  1: Which "binding point" or target the texture is being loaded to.
        //  2: Level of detail. Used for mipmapping. 0 is base texture level.
        //  3: Internal format. The composition of each element, i.e. pixels.
        //  4: Format of texel data. Must match internal format.
        //  5: The data type of the texel data.
        //  6: Texture Data.
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

        // create mipmap
        gl.generateMipmap(gl.TEXTURE_2D);

        // tell webGL we are done manipulating the texture Data
        gl.bindTexture(gl.TEXTURE_2D, null);

        // create our Texture object
        const textureInfo = new TextureInfo(textureName, image.naturalWidth, image.naturalHeight, textureId);

        // pass it to ResourceMap
        gEngine.ResourceMap.asyncLoadCompleted(textureName, textureInfo);
    }

    activateTexture (textureName, isURL) {
        const gl = gEngine.Core.getGL();

        const textureInfo = gEngine.ResourceMap.retrieveAsset(textureName);

        // bind texture reference to webGL buffer
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, textureInfo._gLTextureId);

        // prevent texture wrapping
        gl.getTexParameter(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.getTexParameter(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        // handles how up/down scaling will work
        gl.getTexParameter(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);

        // enable for pixel-perfect rendering when scaling
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    }

    activateDirectTexture (texture) {
        const gl = gEngine.Core.getGL();

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture);

        // prevent texture wrapping
        gl.getTexParameter(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.getTexParameter(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        // handles how up/down scaling will work
        gl.getTexParameter(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);

        // enable for pixel-perfect rendering when scaling
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    }

    activateNormalMap (textureName) {
        const gl = gEngine.Core.getGL();
        const textureInfo = gEngine.ResourceMap.retrieveAsset(textureName);
        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, textureInfo._gLTextureId);

        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        gl.getTexParameter(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    }

    deactivateTexture () {
        const gl = gEngine.Core.getGL();
        gl.bindTexture(gl.TEXTURE_2D, null);
    }

    getColorArray (textureName) {
        const textureInfo = this.getTextureInfo(textureName);
        if (textureInfo._colorArray === null) {
            // create a framebuffer and bind it to the texture and read the color content
            let gl = gEngine.Core.getGL();
            let framebuffer = gl.createFramebuffer();
            gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
            gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, textureInfo._gLTextureId, 0);

            if (gl.checkFramebufferStatus(gl.FRAMEBUFFER) === gl.FRAMEBUFFER_COMPLETE) {
                let pixels = new Uint8Array(textureInfo._width * textureInfo._height * 4);
                gl.readPixels(0, 0, textureInfo._width, textureInfo._height, gl.RGBA, gl.UNSIGNED_BYTE, pixels);
                textureInfo._colorArray = pixels;
            } else {
                alert("WARNING: Engine.Textures.getColorArray() failed!");
            }

            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
            gl.deleteFramebuffer(framebuffer);
        }
        return textureInfo._colorArray;
    }

    getTextureInfo (textureName) {
        return gEngine.ResourceMap.retrieveAsset(textureName);
    }
}
