let instance = null;

export default class Input {
    constructor () {
        this.keys = {
            Left: 37,
            Up: 38,
            Right: 39,
            Down: 40,

            Enter: 13,
            Space: 32,
            Backspace: 8,
            Tab: 9,
            CapsLock: 20,
            Shift: 16,
            Control: 17,
            Alt: 18,
            Escape: 27,
            LeftWindowKey: 91,
            RightWindowKey: 92,

            NumericEnter: 93,
            Numpad0: 96,
            Numpad1: 97,
            Numpad2: 98,
            Numpad3: 99,
            Numpad4: 100,
            Numpad5: 101,
            Numpad6: 102,
            Numpad7: 103,
            Numpad8: 104,
            Numpad9: 105,

            F1: 112,
            F2: 113,
            F3: 114,
            F4: 115,
            F5: 116,
            F6: 117,
            F7: 118,
            F8: 119,
            F9: 120,
            F10: 121,
            F11: 122,
            F12: 123,

            Zero: 48,
            One: 49,
            Two: 50,
            Three: 51,
            Four: 52,
            Five: 53,
            Six: 54,
            Seven: 55,
            Eight: 56,
            Nine: 57,

            A: 65,
            B: 66,
            C: 67,
            D: 68,
            E: 69,
            F: 70,
            G: 71,
            H: 72,
            I: 73,
            J: 74,
            K: 75,
            L: 76,
            M: 77,
            N: 78,
            O: 79,
            P: 80,
            Q: 81,
            R: 82,
            S: 83,
            T: 84,
            U: 85,
            V: 86,
            W: 87,
            X: 88,
            Y: 89,
            Z: 90,

            SemiColon: 186,
            EqualSign: 187,
            Comma: 188,
            Dash: 189,
            Period: 190,
            ForwardSlash: 191,
            GraveAccent: 192,
            OpenBracket: 219,
            BackSlash: 220,
            CloseBracket: 221,
            SingleQuote: 222,

            LastKeyCode: 222
        };

        this._keyPreviousState = [];
        this._isKeyPressed = [];
        this._isKeyClicked = [];
        this._isKeyLetGo = [];

        this._touchPreviousState = false;
        this._isTouchClicked = false;
        this._isTouchPressed = false;
        this._isTouchLetGo = false;
        this._touchPositionX = 0;
        this._touchPositionY = 0;

        this.mouseButton = {
            Left: 0,
            Middle: 1,
            Right: 2
        };

        this._canvas = null;
        this._mouseButtonPreviousState = [];
        this._isMouseButtonPressed = [];
        this._isMouseButtonClicked = [];
        this._isMouseButtonLetGo = [];
        this._mousePositionX = -1;
        this._mousePositionY = -1;

        if (!instance) {
            instance = this;
        }
        return instance;
    }

    // keys

    _onKeyDown (event) {
        instance._isKeyPressed[event.keyCode] = true;
    }
    _onKeyUp (event) {
        instance._isKeyPressed[event.keyCode] = false;
    }

    isKeyPressed (keyCode) {
        return instance._isKeyPressed[keyCode];
    }
    isKeyClicked (keyCode) {
        return instance._isKeyClicked[keyCode];
    }
    isKeyLetGo (keyCode) {
        return instance._isKeyLetGo[keyCode];
    }

    // touch

    _onTouchStart (event) {
        if (instance._onTouchMove(event)) {
            instance._isTouchPressed = true;
        }
    }

    _onTouchMove (event) {
        let inside = false;
        let boundingBox = instance._canvas.getBoundingClientRect();

        // convert from canvas space to client
        let x = Math.round((event.touches[0].clientX - boundingBox.left) * (instance._canvas.width / boundingBox.width));
        let y = Math.round((event.touches[0].clientY - boundingBox.top) * (instance._canvas.width / boundingBox.width));

        if ((x >= 0) && (x < instance._canvas.width) && (y >= 0) && (y < instance._canvas.height)) {
            instance._touchPositionX = x;
            instance._touchPositionY = instance._canvas.height - 1 - y;
            inside = true;
        }
        return inside;
    }

    _onTouchEnd (event) {
        instance._isTouchPressed = false;
    }

    _onTouchCancel (event) {
        instance._isTouchPressed = false;
    }

    isTouchClicked () {
        return instance._isTouchClicked;
    }

    isTouchPressed () {
        return instance._isTouchPressed;
    }

    isTouchLetGo () {
        return instance._isTouchLetGo;
    }

    getTouchPositionX () {
        return instance._touchPositionX;
    }

    getTouchPositionY () {
        return instance._touchPositionY;
    }

    // mouse

    _onMouseMove (event) {
        let inside = false;
        let boundingBox = instance._canvas.getBoundingClientRect();

        // convert from canvas space to client
        let x = Math.round((event.clientX - boundingBox.left) * (instance._canvas.width / boundingBox.width));
        let y = Math.round((event.clientY - boundingBox.top) * (instance._canvas.width / boundingBox.width));

        if ((x >= 0) && (x < instance._canvas.width) && (y >= 0) && (y < instance._canvas.height)) {
            instance._mousePositionX = x;
            instance._mousePositionY = instance._canvas.height - 1 - y;
            inside = true;
        }
        return inside;
    }

    _onMouseDown (event) {
        if (instance._onMouseMove(event)) {
            instance._isMouseButtonPressed[event.button] = true;
        }
    }

    _onMouseUp (event) {
        instance._onMouseMove(event);
        instance._isMouseButtonPressed[event.button] = false;
    }

    isMouseButtonPressed (button) {
        return instance._isMouseButtonPressed[button];
    }

    isMouseButtonClicked (button) {
        return instance._isMouseButtonClicked[button];
    }

    isMouseButtonLetGo (button) {
        return instance._isMouseButtonLetGo[button];
    }

    getMousePositionX () {
        return instance._mousePositionX;
    }

    getMousePositionY () {
        return instance._mousePositionY;
    }

    // initialize and update
    initialize (canvasID) {
        // keyboard support
        for (let i = 0; i < instance.keys.LastKeyCode; i++) {
            instance._isKeyPressed[i] = false;
            instance._keyPreviousState[i] = false;
            instance._isKeyClicked[i] = false;
            instance._isKeyLetGo[i] = false;
        }

        window.addEventListener("keyup", instance._onKeyUp);
        window.addEventListener("keydown", instance._onKeyDown);

        // mouse support
        for (let i = 0; i < 3; i++) {
            instance._mouseButtonPreviousState[i] = false;
            instance._isMouseButtonPressed[i] = false;
            instance._isMouseButtonClicked[i] = false;
            instance._isMouseButtonLetGo[i] = false;
        }
        window.addEventListener("mousedown", instance._onMouseDown);
        window.addEventListener("mouseup", instance._onMouseUp);
        window.addEventListener("mousemove", instance._onMouseMove);
        instance._canvas = document.getElementById(canvasID);

        // touch support
        window.addEventListener("touchstart", instance._onTouchStart);
        window.addEventListener("touchmove", instance._onTouchMove);
        window.addEventListener("touchend", instance._onTouchEnd);
    }

    update () {
        // keyboard
        for (let i = 0; i < instance.keys.LastKeyCode; i++) {
            instance._isKeyClicked[i] = !instance._keyPreviousState[i] && instance._isKeyPressed[i];
            instance._isKeyLetGo[i] = instance._keyPreviousState[i] && !instance._isKeyPressed[i];
            instance._keyPreviousState[i] = instance._isKeyPressed[i];
        }
        // mouse
        for (let i = 0; i < 3; i++) {
            instance._isMouseButtonClicked[i] = !instance._mouseButtonPreviousState[i] && instance._isMouseButtonPressed[i];
            instance._isMouseButtonLetGo[i] = instance._mouseButtonPreviousState[i] && !instance._isMouseButtonPressed[i];
            instance._mouseButtonPreviousState[i] = instance._isMouseButtonPressed[i];
        }
        // touch
        instance._isTouchClicked = !instance._touchPreviousState && instance._isTouchPressed;
        instance._isTouchLetGo = instance._touchPreviousState && !instance._isTouchPressed;
        instance._touchPreviousState = instance._isTouchPressed;
    }
}
