let instance = null;

export default class Random {
    constructor () {
        if (!instance) {
            instance = this;
        }
        return instance;
    }

    randomFloat (min, max, decimals = null) {
        let random = Math.random() * (max - min) + min;
        if (decimals !== null) {
            random = Number(random.toPrecision(decimals));
        }
        return random;
    }

    randomInteger (min, max) {
        return Math.floor(instance.randomFloat(min, max));
    }

    randomNormalizedColorWithAlpha (alpha) {
        const r = Math.random();
        const g = Math.random();
        const b = Math.random();
        return [r, g, b, alpha];
    }

    randomColorWithAlpha (alpha) {
        const r = 255 * Math.random();
        const g = 255 * Math.random();
        const b = 255 * Math.random();
        return [r, g, b, alpha];
    }

    shuffleArray (array) {
        let elements = array.length;
        let i, cache;
        while (elements) {
            i = Math.floor(Math.random() * elements--);

            cache = array[elements];
            array[elements] = array[i];
            array[i] = cache;
        }
        return array;
    }
}
