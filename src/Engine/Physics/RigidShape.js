import gEngine from "../../GameEngine.js";
import { LineRenderable } from "../../GameEngine.js";

export default class RigidShape {
    constructor (transform) {
        this.kPadding = 0.25;
        this._transform = transform;
        this._positionMark = new LineRenderable();
        this._drawBounds = false;

        // physical properties
        this._inversedMass = 1;
        this._restitution = 0.8;
        this._velocity = vec2.fromValues(0, 0);
        this._friction = 0.3;
        this._acceleration = gEngine.Physics.getSystemAcceleration();
    }

    draw (camera) {
        if (!this._drawBounds) {
            return;
        }

        const x = this._transform.getXPosition();
        const y = this._transform.getYPosition();

        this._positionMark.setFirstVertex(x - this.kPadding, y + this.kPadding);
        this._positionMark.setSecondVertex(x + this.kPadding, y - this.kPadding);
        this._positionMark.draw(camera);

        this._positionMark.setFirstVertex(x + this.kPadding, y + this.kPadding);
        this._positionMark.setSecondVertex(x - this.kPadding, y - this.kPadding);
        this._positionMark.draw(camera);
    }

    rigidType () {
        return RigidShape.eRigidType.eRigidAbstract;
    }

    getPosition () {
        return this._transform.getPosition();
    }

    setPosition (x, y) {
        this._transform.setPosition(x, y);
    }

    getTransform () {
        return this._transform;
    }

    setTransform (transform) {
        this._transform = transform;
    }

    getColor () {
        return this._positionMark.getColor();
    }

    setColor (color) {
        this._positionMark.setColor(color);
    }

    getDrawBounds () {
        return this._drawBounds;
    }

    setDrawBounds (drawBounds) {
        this._drawBounds = drawBounds;
    }
}

RigidShape.eRigidType = Object.freeze({
    eRigidAbstract: 0,
    eRigidCircle: 1,
    eRigidRectangle: 2
});
