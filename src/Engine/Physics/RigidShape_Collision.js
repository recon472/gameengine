import gEngine from "../../GameEngine.js";
import { RigidShape } from "../../GameEngine.js";

export default null;

RigidShape.prototype.resolveParticleCollision = function (particle) {
    let status = false;
    switch (this.rigidType()) {
        case RigidShape.eRigidType.eRigidCircle:
            status = gEngine.Particles.resolveCircleParticle(this, particle);
            break;
        case RigidShape.eRigidType.eRigidRectangle:
            status = gEngine.Particles.resolveRectangleParticle(this, particle);
            break;
    }
    return status;
};

RigidShape.prototype.collidedRectangleCircle = function (rectangle, circle, collisionInfo) {
    const rectanglePosition = rectangle.getPosition();
    const circlePosition = circle.getPosition();
    let vectorFrom1to2 = [0, 0];
    vec2.subtract(vectorFrom1to2, circlePosition, rectanglePosition);

    let vector = vec2.clone(vectorFrom1to2);

    const alongX = rectangle.getWidth() / 2;
    const alongY = rectangle.getHeight() / 2;

    vector[0] = Math.clamp(vector[0], -alongX, alongX);
    vector[1] = Math.clamp(vector[1], -alongY, alongY);

    let isInside = false;
    if (rectangle.containsPosition(circlePosition)) {
        isInside = true;
        if (Math.abs(vectorFrom1to2[0] - alongX) < Math.abs(vectorFrom1to2[1] - alongY)) {
            if (vector[0] > 0) {
                vector[0] = alongX;
            } else {
                vector[0] = -alongX;
            }
        } else {
            if (vector[1] > 0) {
                vector[1] = alongY;
            } else {
                vector[1] = -alongY;
            }
        }
    }

    let normal = [0, 0];
    vec2.subtract(normal, vectorFrom1to2, vector);

    const distanceSquared = vec2.squaredLength(normal);
    const radiusSquared = circle.getRadius() * circle.getRadius();

    if (distanceSquared > radiusSquared && !isInside) {
        return false;
    }

    let length = Math.sqrt(distanceSquared);
    let depth;
    vec2.scale(normal, normal, 1 / length);

    if (isInside) {
        vec2.scale(normal, normal, -1);
        depth = circle.getRadius() + length;
    } else {
        depth = circle.getRadius() - length;
    }

    collisionInfo.setNormal(normal);
    collisionInfo.setDepth(depth);
    return true;
};
