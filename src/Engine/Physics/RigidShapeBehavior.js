import gEngine from "../../GameEngine.js";
import { RigidShape } from "../../GameEngine.js";

export default null;

RigidShape.prototype.update = function () {
    const deltaTime = gEngine.GameLoop.getUpdateIntervalInSeconds();

    // sympletic euler
    let velocity = this.getVelocity();
    vec2.scaleAndAdd(velocity, velocity, this._acceleration, (this.getInversedMass() * deltaTime));
    let position = this.getPosition();
    let gameSpeedVelocity = vec2.clone(velocity);
    vec2.scale(gameSpeedVelocity, gameSpeedVelocity, gEngine.Time.getGameSpeed());
    vec2.scaleAndAdd(position, position, gameSpeedVelocity, deltaTime);
};

RigidShape.prototype.getInversedMass = function () {
    return this._inversedMass;
};

RigidShape.prototype.setMass = function (mass) {
    if (mass > 0) {
        this._inversedMass = 1 / mass;
    } else {
        this._inversedMass = 0;
    }
};

RigidShape.prototype.getVelocity = function () {
    return this._velocity;
};

RigidShape.prototype.setVelocity = function (velocity) {
    this._velocity = velocity;
};

RigidShape.prototype.getRestitution = function () {
    return this._restitution;
};

RigidShape.prototype.setRestitution = function (value) {
    this._restitution = value;
};

RigidShape.prototype.getFriction = function () {
    return this._friction;
};

RigidShape.prototype.setFriction = function (value) {
    this._friction = value;
};

RigidShape.prototype.getAcceleration = function () {
    return this._acceleration;
};

RigidShape.prototype.setAcceleration = function (value) {
    this._acceleration = value;
};
