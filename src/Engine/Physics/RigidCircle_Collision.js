import { RigidShape, RigidCircle } from "../../GameEngine.js";

export default null;

RigidCircle.prototype.collidedCircleCircle = function (circle1, circle2, collisionInfo) {
    let vectorFrom1to2 = [0, 0];
    vec2.sub(vectorFrom1to2, circle2.getPosition(), circle1.getPosition());
    let radiusSum = circle1.getRadius() + circle2.getRadius();
    let squareLength = vec2.squaredLength(vectorFrom1to2);
    if (squareLength > (radiusSum * radiusSum)) {
        return false;
    }
    let distance = Math.sqrt(squareLength);
    if (distance !== 0) {
        vec2.scale(vectorFrom1to2, vectorFrom1to2, 1 / distance);
        collisionInfo.setNormal(vectorFrom1to2);
        collisionInfo.setDepth(radiusSum - distance);
    } else {
        collisionInfo.setNormal([0, 1]);
        collisionInfo.setDepth(radiusSum / 10);
    }
    return true;
};

RigidCircle.prototype.collided = function (otherShape, collisionInfo) {
    let status = false;
    let n;
    collisionInfo.setDepth(0);
    switch (otherShape.rigidType()) {
        case RigidShape.eRigidType.eRigidCircle:
            status = this.collidedCircleCircle(this, otherShape, collisionInfo);
            break;
        case RigidShape.eRigidType.eRigidRectangle:
            status = this.collidedRectangleCircle(otherShape, this, collisionInfo);
            n = collisionInfo.getNormal();
            n[0] = -n[0];
            n[1] = -n[1];
            break;
    }
    return status;
};

RigidCircle.prototype.containsPosition = function (position) {
    const distance = vec2.distance(this.getPosition(), position);
    return (distance < this.getRadius());
};
