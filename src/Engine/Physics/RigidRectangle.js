import { RigidShape, LineRenderable } from "../../GameEngine.js";

export default class RigidRectangle extends RigidShape {
    constructor (transform, width, height) {
        super(transform);

        this._sides = new LineRenderable();
        this._width = width;
        this._height = height;
    }

    draw (camera) {
        if (!this._drawBounds) {
            return ;
        }

        super.draw(camera);

        const x = this.getPosition()[0];
        const y = this.getPosition()[1];
        const width = this._width / 2;
        const height = this._height / 2;

        this._sides.setFirstVertex(x - width, y + height);
        this._sides.setSecondVertex(x + width, y + height);
        this._sides.draw(camera);

        this._sides.setFirstVertex(x + width, y - height);
        this._sides.draw(camera);

        this._sides.setSecondVertex(x - width, y - height);
        this._sides.draw(camera);

        this._sides.setFirstVertex(x - width, y + height);
        this._sides.draw(camera);
    }

    rigidType () {
        return RigidShape.eRigidType.eRigidRectangle;
    }

    getWidth () {
        return this._width;
    }

    getHeight () {
        return this._height;
    }

    setColor (color) {
        super.setColor(color);
        this._sides.setColor(color);
    }
}
