import { RigidShape, LineRenderable } from "../../GameEngine.js";

export default class RigidCircle extends RigidShape {
    constructor (transform, radius) {
        super(transform);

        this.kNumberOfSides = 16;
        this._sides = new LineRenderable();
        this._radius = radius;
    }

    draw (camera) {
        if (!this._drawBounds) {
            return;
        }

        super.draw(camera);

        const position = this.getPosition();
        let previousPoint = vec2.clone(position);
        const deltaTheta = (Math.PI * 2) / this.kNumberOfSides;
        let theta = deltaTheta;
        previousPoint[0] += this._radius;

        let i, x, y;
        for (i = 0; i < this.kNumberOfSides; i++) {
            x = position[0] + this._radius * Math.cos(theta);
            y = position[1] + this._radius * Math.sin(theta);

            this._sides.setFirstVertex(previousPoint[0], previousPoint[1]);
            this._sides.setSecondVertex(x, y);
            this._sides.draw(camera);

            theta = theta + deltaTheta;
            previousPoint[0] = x;
            previousPoint[1] = y;
        }
    }

    rigidType () {
        return RigidShape.eRigidType.eRigidCircle;
    }

    getRadius () {
        return this._radius;
    }

    setColor (color) {
        super.setColor(color);
        this._sides.setColor(color);
    }
}
