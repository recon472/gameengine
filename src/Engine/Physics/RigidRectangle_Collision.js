import { RigidRectangle, RigidShape } from "../../GameEngine.js";

export default null;

RigidRectangle.prototype.collidedRectangleRectangle = function (rectangle1, rectangle2, collisionInfo) {
    let vectorFrom1to2 = vec2.fromValues(0, 0);
    vec2.sub(vectorFrom1to2, rectangle2.getPosition(), rectangle1.getPosition());
    let xDepth = (rectangle1.getWidth() / 2) + (rectangle2.getWidth() / 2) - Math.abs(vectorFrom1to2[0]);
    if (xDepth > 0) {
        let yDepth = (rectangle1.getHeight() / 2) + (rectangle2.getHeight() / 2) - Math.abs(vectorFrom1to2[1]);

        if (yDepth > 0) {
            if (xDepth < yDepth) {
                if (vectorFrom1to2[0] < 0) {
                    collisionInfo.setNormal([-1, 0]);
                } else {
                    collisionInfo.setNormal([1, 0]);
                }
                collisionInfo.setDepth(xDepth);
            } else {
                if (vectorFrom1to2[1] < 0) {
                    collisionInfo.setNormal([0, -1]);
                } else {
                    collisionInfo.setNormal([0, 1]);
                }
                collisionInfo.setDepth(yDepth);
            }
            return true;
        }
    }
    return false;
};

RigidRectangle.prototype.collided = function (otherShape, collisionInfo) {
    let status = false;
    collisionInfo.setDepth(0);
    switch (otherShape.rigidType()) {
        case RigidShape.eRigidType.eRigidCircle:
            status = this.collidedRectangleCircle(this, otherShape, collisionInfo);
            break;
        case RigidShape.eRigidType.eRigidRectangle:
            status = this.collidedRectangleRectangle(this, otherShape, collisionInfo);
            break;
    }
    return status;
};

RigidRectangle.prototype.containsPosition = function (position) {
    const rectanglePosition = this.getPosition();
    const rectangleMinX = rectanglePosition[0] - this.getWidth() / 2;
    const rectangleMaxX = rectanglePosition[0] + this.getWidth() / 2;
    const rectangleMinY = rectanglePosition[1] - this.getHeight() / 2;
    const rectangleMaxY = rectanglePosition[1] + this.getHeight() / 2;

    return ((rectangleMinX < position[0]) && (rectangleMaxX > position[0]) &&
            (rectangleMinY < position[1]) && (rectangleMaxY > position[1]));
};
