export default class Scene {
    initialize () { }
    loadScene () { }
    unloadScene () { }
    update () { }
    windowResized () { }

    userConnected (userID) { }
    userDisconnected (userID) { }

    getGameWorld () {
        return this._gameWorld;
    }
}
