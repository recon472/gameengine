import { SpriteRenderable } from "../../GameEngine.js";

export default null;

SpriteRenderable.prototype._setTextureInfo = function () {
    let imageWidth = this._textureInfo._width;
    let imageHeight = this._textureInfo._height;

    this._textureLeftIndex = this._textureLeft * imageWidth;
    this._textureBottomIndex = this._textureBottom * imageHeight;

    this._textureWidth = ((this._textureRight - this._textureLeft) * imageWidth) + 1;
    this._textureHeight = ((this._textureTop - this._textureBottom) * imageHeight) + 1;
}
