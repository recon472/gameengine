import gEngine from "../../GameEngine.js";
import { Renderable } from "../../GameEngine.js";

export default class DirectTextureRenderable extends Renderable {
    constructor (texture, ignoreAmbient = true) {
        super();

        super.setColor([1, 1, 1, 0]);
        super._setShader(gEngine.DefaultResources.getTextureShader());
        this._shader.ignoreAmbient = ignoreAmbient;

        // catch texture information for per-pixel collision
        this._textureInfo = null;
        this._colorArray = null;

        // define for subclass to override
        this._textureWidth = 0;
        this._textureHeight = 0;
        this._textureLeftIndex = 0;
        this._textureBottomIndex = 0;

        this._texture = texture;
    }

    draw (camera) {
        if (this._texture == null || this._texture == undefined) {
            return;
        }

        gEngine.Textures.activateDirectTexture(this._texture);
        super.draw(camera);
    }

    setTexture (texture, width, height) {
        this._texture = texture;
        this._colorArray = null;
        this._textureWidth = width;
        this._textureHeight = height;
        this._textureLeftIndex = 0;
        this._textureBottomIndex = 0;
    }

    getTexture () {
        return this._texture;
    }
}
