import gEngine from "../../GameEngine.js";
import { Renderable } from "../../GameEngine.js";

export default class TextRenderable extends Renderable {
    constructor (text = "text", color = "white", size = 20, scale = 0.5, font = "monospace", ignoreAmbient = true) {
        super();

        super.setColor([0, 0, 0, 0]);
        super._setShader(gEngine.DefaultResources.getTextureShader());
        this._shader.ignoreAmbient = ignoreAmbient;

        this._texture = null;

        this._text = text;
        this._textColor = color;
        this._size = size;
        this._scale = scale;
        this._font = font;
        this._alignment = TextRenderable.eTextAlignment.Center;
        this._position = vec2.fromValues(0, 0);

        this.setText(text, color, size, scale, font);
    }

    setText (text = this._text, color = this._textColor, size = this._size, scale = this._scale, font = this._font) {
        gEngine.ProceduralTexture.fromText(text, color, size, font, this._alignment, (texture, width, height) => {
            this._texture = texture;

            let ratio = width / height;
            height = this._size * (this._scale || 1);
            this.getTransform().setSize(height * ratio, height);

            let actualWidth = this.getTransform().getWidth();
            let x = this._position[0];
            let y = this._position[1];
            switch (this._alignment) {
                case TextRenderable.eTextAlignment.Left:
                    x = x + actualWidth / 2;
                    break;
                case TextRenderable.eTextAlignment.Right:
                    x = x - actualWidth / 2;
                    break;
            }

            this.getTransform().setPosition(x, y);
        });
    }

    setPosition (x, y, alignment = this._alignment) {
        this._alignment = alignment;
        this._position[0] = x;
        this._position[1] = y;
        this.setText();
    }

    draw (camera) {
        if (this._texture == null || this._texture == undefined) {
            return;
        }

        gEngine.Textures.activateDirectTexture(this._texture);
        super.draw(camera);
    }
}

TextRenderable.eTextAlignment = Object.freeze({
    Left: "left",
    Center: "center",
    Right: "right"
});
