import gEngine from "../../GameEngine.js";
import { Renderable } from "../../GameEngine.js";

export default class LineRenderable extends Renderable {
    constructor (startX, startY, endX, endY) {
        super();
        super.setColor([0, 0, 0, 1]);
        super._setShader(gEngine.DefaultResources.getLineShader());

        this._pointSize = 1;
        this._drawVertices = false;
        this._showLine = true;

        this._startPosition = vec2.fromValues(0, 0);
        this._endPosition = vec2.fromValues(0, 0);

        if (startX !== undefined) {
            this.setVertices(startX, startY, endX, endY);
        }
    }

    draw (camera) {
        this._shader.setPointSize(this._pointSize);

        let gl = gEngine.Core.getGL();
        this._shader.activateShader(this._color, camera);

        let sizeX = this._startPosition[0] - this._endPosition[0];
        let sizeY = this._startPosition[1] - this._endPosition[1];
        let centerX = this._startPosition[0] - sizeX / 2;
        let centerY = this._startPosition[1] - sizeY / 2;
        let transform = this.getTransform();
        transform.setSize(sizeX, sizeY);
        transform.setPosition(centerX, centerY);

        this._shader.loadObjectTransform(this._transform.getTransform());
        if (this._showLine) {
            gl.drawArrays(gl.LINE_STRIP, 0, 2);
        }
        if (!this._showLine || this._drawVertices) {
            gl.drawArrays(gl.POINTS, 0, 2);
        }
    }

    setVertices (startX, startY, endX, endY) {
        this.setFirstVertex(startX, startY);
        this.setSecondVertex(endX, endY);
    }

    setFirstVertex (x, y) {
        this._startPosition[0] = x;
        this._startPosition[1] = y;
    }

    setSecondVertex (x, y) {
        this._endPosition[0] = x;
        this._endPosition[1] = y;
    }

    setDrawVertices (draw) {
        this._drawVertices = draw;
    }

    setShowLine (show) {
        this._showLine = show;
    }

    setPointSize (width) {
        this._pointSize = width;
    }
}
