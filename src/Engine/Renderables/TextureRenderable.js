import gEngine from "../../GameEngine.js";
import { Renderable } from "../../GameEngine.js";

export default class TextureRenderable extends Renderable {
    constructor (texture) {
        super();

        super.setColor([1, 1, 1, 0]);
        super._setShader(gEngine.DefaultResources.getTextureShader());

        this._texture = null;

        // catch texture information for per-pixel collision
        this._textureInfo = null;
        this._colorArray = null;

        // define for subclass to override
        this._textureWidth = 0;
        this._textureHeight = 0;
        this._textureLeftIndex = 0;
        this._textureBottomIndex = 0;

        this.setTexture(texture);
    }

    draw (camera) {
        gEngine.Textures.activateTexture(this._texture);
        super.draw(camera);
    }

    getTexture () {
        return this._texture;
    }

    setTexture (texture) {
        this._texture = texture;
        this._textureInfo = gEngine.Textures.getTextureInfo(texture);
        this._colorArray = null;
        this._textureWidth = this._textureInfo._width;
        this._textureHeight = this._textureInfo._height;
        this._textureLeftIndex = 0;
        this._textureBottomIndex = 0;
        // this.setColorArray();
    }
}
