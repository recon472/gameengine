import gEngine from "../../GameEngine.js";
import {
    LightRenderable, Material
} from "../../GameEngine.js";

export default class IlluminationRenderable extends LightRenderable {
    constructor (texture, normalMap) {
        super(texture);
        super._setShader(gEngine.DefaultResources.getIlluminationShader());

        this._normalMap = normalMap;
        this._material = new Material();
    }

    draw (camera) {
        gEngine.Textures.activateNormalMap(this._normalMap);
        this._shader.setMaterialAndCameraPosition(this._material, camera.getPositionInPixelSpace());
        super.draw(camera);
    }

    getMaterial () {
        return this._material;
    }
}
