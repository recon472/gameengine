import gEngine from "../../GameEngine.js";
import {
    GameComponent, Transform
} from "../../GameEngine.js";

export default class Renderable extends GameComponent {
    constructor () {
        super();
        this._shader = gEngine.DefaultResources.getConstantColorShader();
        this._color = [1, 1, 1, 1]; // color for FS shader
        this._transform = new Transform();
    }

    draw (camera) {
        let gl = gEngine.Core.getGL();
        this._shader.activateShader(this._color, camera);
        this._shader.loadObjectTransform(this._transform.getTransform());
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
    }

    getTransform () {
        return this._transform;
    }

    setColor (color) {
        this._color = color;
    }

    getColor () {
        return this._color;
    }

    _setShader (shader) {
        this._shader = shader;
    }

    swapShader (shader) {
        let out = this._shader;
        this._shader = shader;
        return out;
    }

    setIgnoreAmbient (ignore) {
        this._shader.ignoreAmbient = ignore;
    }
}
