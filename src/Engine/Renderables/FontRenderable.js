export default class FontRenderable {
    constructor (text) {
        this._font = gEngine.DefaultResources.getDefaultFont();
        this._oneCharacter = new SpriteRenderable(this._font + ".png");
        this._transform = new Transform();
        this._text = text;
    }

    draw (camera) {
        let widthOfOneCharacter = this._transform.getWidth() / this._text.length;
        let heightOfOneCharacter = this._transform.getHeight();
        let yPosition = this._transform.getYPosition();

        // center position of the first character
        let xPosition = this._transform.getXPosition() - (widthOfOneCharacter / 2) + (widthOfOneCharacter * 0.5);

        // draw all characters
        let character, characterInfo, xSize, ySize, xOffset, yOffset;
        for (let i = 0; i < this._text.length; i++) {
            // get characterInfo
            character = this._text.charCodeAt(i);
            characterInfo = gEngine.Fonts.getCharacterInfo(this._font, character);

            // set texture coordinate
            this._oneCharacter.setElementUVCoordinate(
                characterInfo._textureCoordinateLeft,
                characterInfo._textureCoordinateRight,
                characterInfo._textureCoordinateBottom,
                characterInfo._textureCoordinateTop);

            // set size of character
            xSize = widthOfOneCharacter * characterInfo._characterWidth;
            ySize = heightOfOneCharacter * characterInfo._characterHeight;
            this._oneCharacter.getTransform().setSize(xSize, ySize);

            // center offset
            xOffset = widthOfOneCharacter * characterInfo._characterWidthOffset * 0.5;
            yOffset = heightOfOneCharacter * characterInfo._characterHeightOffset * 0.5;
            this._oneCharacter.getTransform().setPosition(xPosition - xOffset, yPosition - yOffset);

            this._oneCharacter.draw(camera);
            xPosition += widthOfOneCharacter;
        }
    }

    setTextHeight (height) {
        let characterInfo = gEngine.Fonts.getCharacterInfo(this._font, "A".charCodeAt(0));
        let width = height * characterInfo._characterAspectRatio;
        this.getTransform().setSize(width * this._text.length, height);
    }

    getTransform () {
        return this._transform;
    }

    getText () {
        return this._text;
    }

    setText (text) {
        this._text = text;
        this.setTextHeight(this.getTransform().getHeight());
    }

    getFont () {
        return this._font;
    }

    setFont (font) {
        this._font = font;
        this._oneCharacter.setTexture(this._font + ".png");
    }

    getColor () {
        return this._oneCharacter.getColor();
    }

    setColor (color) {
        this._oneCharacter.setColor(color);
    }
}
