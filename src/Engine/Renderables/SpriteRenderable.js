import gEngine from "../../GameEngine.js";
import { TextureRenderable} from "../../GameEngine.js";

export default class SpriteRenderable extends TextureRenderable {
    constructor (texture) {
        super(texture);
        super._setShader(gEngine.DefaultResources.getSpriteShader());

        // image bounds
        this._textureLeft = 0.0;   // 0 is left, 1 is right
        this._textureRight = 1.0;
        this._textureTop = 1.0;    // 1 is top, 0 is bottom
        this._textureBottom = 0.0;

        this._setTextureInfo();
    }

    setElementPixelPositions (left, right, bottom, top) {
        let textureInfo = gEngine.ResourceMap.retrieveAsset(this._texture);

        // entire image size
        let imageWidth = textureInfo._width;
        let imageHeight = textureInfo._height;

        // convert pixels to normalized values
        this._textureLeft = left / imageWidth;
        this._textureRight = right / imageWidth;
        this._textureBottom = bottom / imageHeight;
        this._textureTop = top /imageHeight;

        this._setTextureInfo();
    }

    getElementUVCoordinateArray () {
        return [
            this._textureRight,  this._textureTop,
            this._textureLeft,   this._textureTop,
            this._textureRight,  this._textureBottom,
            this._textureLeft,   this._textureBottom
        ];
    }

    setElementUVCoordinate (left, right, bottom, top) {
        this._textureLeft = left;
        this._textureRight = right;
        this._textureTop = top;
        this._textureBottom = bottom;

        this._setTextureInfo();
    }

    draw (camera) {
        this._shader.setTextureCoordinates(this.getElementUVCoordinateArray());
        super.draw(camera);
    }
}

//  [0] [1]: is u/v cooridnate of Top-Right
//  [2] [3]: is u/v coordinate of Top-Left
//  [4] [5]: is u/v coordinate of Bottom-Right
//  [6] [7]: is u/v coordinate of Bottom-Left
SpriteRenderable.eTextureCoordinateArray = Object.freeze({
    eLeft: 2,
    eRight: 0,
    eTop: 1,
    eBottom: 5
});
