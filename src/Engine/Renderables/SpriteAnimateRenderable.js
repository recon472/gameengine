import gEngine from "../../GameEngine.js";
import { SpriteRenderable } from "../../GameEngine.js";

export default class SpriteAnimateRenderable extends SpriteRenderable {
    constructor (texture, playOnceAndDestroy = false) {
        super(texture);
        super._setShader(gEngine.DefaultResources.getSpriteShader());

        // all coordinates are uv normalized
        this._firstElementLeft = 0.0;
        this._firstElementTop = 1.0;
        this._elementWidth = 1.0;
        this._elementHeight = 1.0;
        this._horizontalPadding = 0.0;
        this._verticalPadding = 0.0;
        this._numberOfElements = 1;
        this._numberOfElementsInLine = 1;

        // animation settings
        this._horizontalAnimationType = SpriteAnimateRenderable.eHorizontalAnimationType.eAnimateRight;
        this._verticalAnimationType = SpriteAnimateRenderable.eVerticalAnimationType.eAnimateDown;
        this._updateInterval = 1;
        this._swapAxes = false;
        this._alternateDirection = false;
        this._animate = false;
        this._playOnceAndDestroy = playOnceAndDestroy;

        // current animation state
        this._currentAnimationAdvance = -1;
        this._currentElement = 0;
        this._initAnimation();
    }

    setAnimationType (horizontalAnimationType, verticalAnimationType, swapAxes, alternateDirection) {
        this._horizontalAnimationType = horizontalAnimationType;
        if (verticalAnimationType !== null && verticalAnimationType !== undefined) {
            this._verticalAnimationType = verticalAnimationType;
        }
        if (swapAxes !== null && swapAxes !== undefined) {
            this._swapAxes = swapAxes;
        }
        if (alternateDirection !== null && alternateDirection !== undefined) {
            this._alternateDirection = alternateDirection;
        }
        this._currentAnimationAdvance = -1;
        this._currentElement = 0;
        this._initAnimation();
    }

    _initAnimation () {
        this._currentTick = 0;
        switch (this._horizontalAnimationType) {
            case SpriteAnimateRenderable.eHorizontalAnimationType.eAnimateRight:
                this._currentElement = 0;
                this._currentAnimationAdvance = 1;
                break;
            case SpriteAnimateRenderable.eHorizontalAnimationType.eAnimateSwing:
                this._currentAnimationAdvance = -1 * this._currentAnimationAdvance;
                this._currentElement += 2 * this._currentAnimationAdvance;
                break;
            case SpriteAnimateRenderable.eHorizontalAnimationType.eAnimateLeft:
                this._currentElement = this._numberOfElements - 1;
                this._currentAnimationAdvance = -1;
                break;
        }
        this._setSpriteElement();
    }

    _setSpriteElement () {
        let row = Math.floor(this._currentElement / this._numberOfElementsInLine) + 1;
        let column = this._currentElement;

        while (column + 1 > this._numberOfElementsInLine) {
            column -= this._numberOfElementsInLine;
        }

        if (this._alternateDirection && row % 2 == 0) {
            column = this._numberOfElementsInLine - column - 1;
        }

        if (this._swapAxes) {
            row = [column + 1, column = row - 1][0];
        }

        if ((this._horizontalAnimationType === SpriteAnimateRenderable.eHorizontalAnimationType.eAnimateLeft &&
            this._verticalAnimationType === SpriteAnimateRenderable.eVerticalAnimationType.eAnimateDown) ||
            (this._horizontalAnimationType === SpriteAnimateRenderable.eHorizontalAnimationType.eAnimateRight &&
                this._verticalAnimationType === SpriteAnimateRenderable.eVerticalAnimationType.eAnimateUp)) {
            row = Math.ceil(this._numberOfElements / this._numberOfElementsInLine) + 1 - row;
        }

        let left = this._firstElementLeft + ((this._elementWidth + this._horizontalPadding) * column);
        let top = this._firstElementTop - ((this._elementHeight + this._verticalPadding) * row);

        super.setElementUVCoordinate(left, left + this._elementWidth, top, top + this._elementHeight);
    }

    // Always set the right-most element to be the first
    setSpriteSequence (topPixel, leftPixel, elementWidthInPixel, elementHeightInPixel, numberOfElements, numberOfElementsInLine, horizontalPaddingInPixel, verticalPaddingInPixel) {
        let textureInfo = gEngine.ResourceMap.retrieveAsset(this._texture);
        let imageWidth = textureInfo._width;
        let imageHeight = textureInfo._height;

        this._numberOfElements = numberOfElements;
        this._numberOfElementsInLine = numberOfElementsInLine;
        this._firstElementLeft = leftPixel / imageWidth;
        this._firstElementTop = topPixel / imageHeight;
        this._elementWidth = elementWidthInPixel / imageWidth;
        this._elementHeight = elementHeightInPixel / imageHeight;
        this._horizontalPadding = horizontalPaddingInPixel / imageWidth;
        this._verticalPadding = verticalPaddingInPixel;
        this._initAnimation();
        this._animate = true;
    }

    setAnimationSpeed (tickInterval) {
        this._updateInterval = tickInterval;
    }

    increaseAnimationSpeed (deltaInterval) {
        this._updateInterval += deltaInterval;
    }

    setAnimate (value) {
        this._animate = value;
    }

    getAnimate (value) {
        return this._animate;
    }

    update () {
        if (!this._animate) {
            return;
        }

        this._currentTick++;
        if (this._currentTick >= this._updateInterval) {
            this._currentTick = 0;
            this._currentElement += this._currentAnimationAdvance;
            if (this._currentElement >= 0 && this._currentElement < this._numberOfElements) {
                this._setSpriteElement();
            } else {
                if (this._playOnceAndDestroy) {
                    this._animate = false;
                    this.gameObject.destroy();
                } else {
                    this._initAnimation();
                }
            }
        }
    }
}

SpriteAnimateRenderable.eHorizontalAnimationType = Object.freeze({
    eAnimateRight: 0,
    eAnimateLeft: 1,
    eAnimateSwing: 2
});

SpriteAnimateRenderable.eVerticalAnimationType = Object.freeze({
    eAnimateUp: 0,
    eAnimateDown: 1
});
