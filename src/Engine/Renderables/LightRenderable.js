import gEngine from "../../GameEngine.js";
import { SpriteAnimateRenderable } from "../../GameEngine.js";

export default class LightRenderable extends SpriteAnimateRenderable {
    constructor (texture) {
        super(texture);
        super._setShader(gEngine.DefaultResources.getLightShader());

        this._lights = [];
    }

    draw (camera) {
        this._shader.setLights(this._lights);
        super.draw(camera);
    }

    getLightAt (index) {
        return this._lights[index];
    }

    addLight (light) {
        this._lights.push(light);
    }

    numberOfLights () {
        return this._lights.length;
    }
}
