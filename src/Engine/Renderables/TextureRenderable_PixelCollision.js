import gEngine from "../../GameEngine.js";
import { TextureRenderable } from "../../GameEngine.js";

export default null;

TextureRenderable.prototype.setColorArray = function () {
    if (this._colorArray === null) {
        this._colorArray = gEngine.Textures.getColorArray(this._texture);
    }
};

TextureRenderable.prototype._pixelAlphaValue = function (x, y) {
    x += this._textureLeftIndex;
    y += this._textureBottomIndex;
    x = x * 4;
    y = y * 4;
    return this._colorArray[(y * this._textureInfo._width) + x + 3];
};

TextureRenderable.prototype._indexToWCPosition = function (returnWCPosition, i, j, xDirection, yDirection) {
    let x = i * this._transform.getWidth() / (this._textureWidth - 1);
    let y = j * this._transform.getHeight() / (this._textureHeight - 1);

    let xDisplacement = x - (this._transform.getWidth() * 0.5);
    let yDisplacement = y - (this._transform.getHeight() * 0.5);
    let xDirectionDisplacement = [];
    let yDirectionDisplacement = [];

    vec2.scale(xDirectionDisplacement, xDirection, xDisplacement);
    vec2.scale(yDirectionDisplacement, yDirection, yDisplacement);
    vec2.add(returnWCPosition, this._transform.getPosition(), xDirectionDisplacement);
    vec2.add(returnWCPosition, returnWCPosition, yDirectionDisplacement);
};

TextureRenderable.prototype._wcPositionToIndex = function (returnIndex, wcPosition, xDirection, yDirection) {
    let delta = [];
    vec2.sub(delta, wcPosition, this._transform.getPosition());
    let xDisplacement = vec2.dot(delta, xDirection);
    let yDisplacement = vec2.dot(delta, yDirection);
    returnIndex[0] = this._textureWidth * (xDisplacement / this._transform.getWidth());
    returnIndex[1] = this._textureHeight * (yDisplacement / this._transform.getHeight());

    // offset center to return lower left corner
    returnIndex[0] += this._textureWidth / 2;
    returnIndex[1] += this._textureHeight / 2;

    returnIndex[0] = Math.floor(returnIndex[0]);
    returnIndex[1] = Math.floor(returnIndex[1]);
};

TextureRenderable.prototype.pixelTouches = function (other, wcTouchPosition) {
    let pixelTouch = false;
    let xIndex = 0;
    let yIndex;
    let otherIndex = [0, 0];

    let xDirection = [1, 0];
    let yDirection = [0, 1];
    let otherXDirection = [1, 0];
    let otherYDirection = [0, 1];
    vec2.rotate(xDirection, xDirection, this._transform.getRotationInRadian());
    vec2.rotate(yDirection, yDirection, this._transform.getRotationInRadian());
    vec2.rotate(otherXDirection, otherXDirection, other._transform.getRotationInRadian());
    vec2.rotate(otherYDirection, otherYDirection, other._transform.getRotationInRadian());

    while ((!pixelTouch) && (xIndex < this._textureWidth)) {
        yIndex = 0;
        while ((!pixelTouch) && (yIndex < this._textureHeight)) {
            if (this._pixelAlphaValue(xIndex, yIndex) > 0) {
                this._indexToWCPosition(wcTouchPosition, xIndex, yIndex, xDirection, yDirection);
                other._wcPositionToIndex(otherIndex, wcTouchPosition, otherXDirection, otherYDirection);
                if ((otherIndex[0] > 0) && (otherIndex[0] < other._textureWidth) &&
                    (otherIndex[1] > 0) && (otherIndex[1] < other._textureHeight)) {
                    pixelTouch = other._pixelAlphaValue(otherIndex[0], otherIndex[1]) > 0;
                }
            }
            yIndex++;
        }
        xIndex++;
    }
    return pixelTouch;
};
