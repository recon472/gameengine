import gEngine from "../../GameEngine.js";
import { TextureRenderable } from "../../GameEngine.js";

export default class ParticleRenderable extends TextureRenderable {
    constructor (texture) {
        super(texture);
        super._setShader(gEngine.DefaultResources.getParticleShader());
    }
}
