export default class Material {
    constructor () {
        this._ka = vec4.fromValues(0.0, 0.0, 0.0, 0);
        this._ks = vec4.fromValues(0.2, 0.2, 0.2, 1);
        this._kd = vec4.fromValues(1.0, 1.0, 1.0, 1);
        this._shininess = 20;
    }

    setAmbient (ambient) {
        this._ka = vec4.clone(ambient);
    }

    getAmbient () {
        return this._ka;
    }

    setDiffuse (diffuse) {
        this._kd = vec4.clone(diffuse);
    }

    getDiffuse () {
        return this._kd;
    }

    setSpecular (specular) {
        this._ks = vec4.clone(specular);
    }

    getSpecular () {
        return this._ks;
    }

    setShininess (shininess) {
        this._shininess = shininess;
    }

    getShininess () {
        return this._shininess;
    }
}
