export default class LightSet {
    constructor () {
        this._set = [];
    }

    numberOfLights () {
        return this._set.length;
    }

    getLightAt (index) {
        return this._set[index];
    }

    addToSet (light) {
        this._set.push(light);
    }
}
