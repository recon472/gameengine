export default class Light {
    constructor () {
        this._color = vec4.fromValues(0.1, 0.1, 0.1, 1);
        this._position = vec3.fromValues(0, 0, 5);
        this._direction = vec3.fromValues(0, 0, -1);
        this._near = 5;
        this._far = 10;
        this._inner = 0.1;
        this._outer = 0.3;
        this._intensity = 1;
        this._dropOff = 1;
        this._lightType = Light.eLightType.ePointLight;
        this._isOn = true;
        this._castShadow = false;
    }

    isLightCastingShadow () {
        return this._castShadow;
    }

    setCastShadow (on) {
        this._castShadow = on;
    }

    setDirection (direction) {
        this._direction = vec3.clone(direction);
    }

    getDirection () {
        return this._direction;
    }

    setInner (inner) {
        this._inner = inner;
    }

    getInner () {
        return this._inner;
    }

    setOuter (outer) {
        this._outer = outer;
    }

    getOuter () {
        return this._outer;
    }

    setDropOff (dropOff) {
        this._dropOff = dropOff;
    }

    getDropOff () {
        return this._dropOff;
    }

    setLightType (lightType) {
        this._lightType = lightType;
    }

    getLightType () {
        return this._lightType;
    }

    setNear (value) {
        this._near = value;
    }

    getNear () {
        return this._near;
    }

    setFar (value) {
        this._far = value;
    }

    getFar () {
        return this._far;
    }

    setIntensity (intensity) {
        this._intensity = intensity;
    }

    getIntensity () {
        return this._intensity;
    }

    setColor (color) {
        this._color = vec4.clone(color);
    }

    getColor () {
        return this._color;
    }

    set2DPosition (position) {
        this._position = vec3.fromValues(position[0], position[1], this._position[2]);
    }

    setXPosition (xPosition) {
        this._position[0] = xPosition;
    }

    setYPosition (yPosition) {
        this._position[1] = yPosition;
    }

    setZPosition (zPosition) {
        this._position[2] = zPosition;
    }

    getPosition () {
        return this._position;
    }

    setLightOn (on) {
        this._isOn = on;
    }

    isLightOn () {
        return this._isOn;
    }
}

Light.eLightType = Object.freeze({
    ePointLight: 0,
    eDirectionalLight: 1,
    eSpotlight: 2
});
