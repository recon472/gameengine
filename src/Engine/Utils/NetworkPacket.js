import gEngine from "../../GameEngine.js";

export default class NetworkPacket {
    constructor () {
        this.clientID = gEngine.Networking.getClientID();
        this.timeStamp = Date.now();

        this.load = null;
        this.update = {};
        this.create = [];
        this.remove = [];
        this.ownership = {};
    }
}
