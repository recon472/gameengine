export default class BoundingBox {
    constructor (centerPosition, width, height) {
        this._lowerLeftCorner = vec2.fromValues(0, 0);
        this.setBounds(centerPosition, width, height);
    }

    setBounds (centerPosition, width, height) {
        this._width = width;
        this._height = height;
        this._lowerLeftCorner[0] = centerPosition[0] - (width / 2);
        this._lowerLeftCorner[1] = centerPosition[1] - (height / 2);
    }

    containsPoint (x, y) {
        return (
            (x > this.minX()) && (x < this.maxX()) &&
            (y > this.minY()) && (y < this.maxY())
        );
    }

    intersectsBound (otherBound) {
        return (
            (this.minX() < otherBound.maxX()) &&
            (this.maxX() > otherBound.minX()) &&
            (this.minY() < otherBound.maxY()) &&
            (this.maxY() > otherBound.minY())
        );
    }

    boundCollideStatus (otherBound) {
        let status = BoundingBox.eBoundCollideStatus.eOutside;

        if (this.intersectsBound(otherBound)) {
            if (otherBound.minX() < this.minX()) {
                status |= BoundingBox.eBoundCollideStatus.eCollideLeft;
            }
            if (otherBound.maxX() > this.maxX()) {
                status |= BoundingBox.eBoundCollideStatus.eCollideRight;
            }
            if (otherBound.minY() < this.minY()) {
                status |= BoundingBox.eBoundCollideStatus.eCollideBottom;
            }
            if (otherBound.maxY() > this.maxY()) {
                status |= BoundingBox.eBoundCollideStatus.eCollideTop;
            }

            if (status === BoundingBox.eBoundCollideStatus.eOutside) {
                status = BoundingBox.eBoundCollideStatus.eInside;
            }
        }
        return status;
    }

    minX () {
        return this._lowerLeftCorner[0];
    }

    maxX () {
        return this._lowerLeftCorner[0] + this._width;
    }

    minY () {
        return this._lowerLeftCorner[1];
    }

    maxY () {
        return this._lowerLeftCorner[1] + this._height;
    }
}

BoundingBox.eBoundCollideStatus = Object.freeze({
    eCollideLeft: 1,
    eCollideRight: 2,
    eCollideTop: 4,
    eCollideBottom: 8,
    eInside: 16,
    eOutside: 0
});
