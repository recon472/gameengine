import { GameComponent } from "../../GameEngine.js";

export default class Transform extends GameComponent {
    constructor () {
        super();
        this._position = vec2.fromValues(0, 0);
        this._scale = vec2.fromValues(1, 1);
        this._rotationInRadian = 0.0;
        this._z = 0.0;
    }

    getTransform () {
        let matrix = mat4.create();
        mat4.translate(matrix, matrix, this.get3DPosition());
        mat4.rotateZ(matrix, matrix, this.getRotationInRadian());
        mat4.scale(matrix, matrix, vec3.fromValues(this.getWidth(), this.getHeight(), 1.0));
        return matrix;
    }

    cloneTo (transform) {
        transform._position = vec2.clone(this._position);
        transform._scale = vec2.clone(this._scale);
        transform._z = this._z;
        transform._rotationInRadian = this._rotationInRadian;
    }

    // setters and getters

    get3DPosition () {
        return vec3.fromValues(this.getXPosition(), this.getYPosition(), this.getZPosition());
    }

    setZPosition (z) {
        this._z = z;
    }

    getZPosition () {
        return this._z;
    }

    increaseZPositionBy (delta) {
        this._z += delta;
    }

    setPosition (xPosition, yPosition) {
        this.setXPosition(xPosition);
        this.setYPosition(yPosition);
    }

    getPosition () {
        return this._position;
    }

    setXPosition (xPosition) {
        this._position[0] = xPosition;
    }

    getXPosition () {
        return this._position[0];
    }

    setYPosition (yPosition) {
        this._position[1] = yPosition;
    }

    getYPosition () {
        return this._position[1];
    }

    increaseXPositionBy (delta) {
        this._position[0] += delta;
    }

    increaseYPositionBy (delta) {
        this._position[1] += delta;
    }

    setSize (width, height) {
        this.setWidth(width);
        this.setHeight(height);
    }

    getSize () {
        return this._scale;
    }

    increaseSizeBy (delta) {
        this.increaseWidthBy(delta);
        this.increaseHeightBy(delta);
    }

    setWidth (width) {
        this._scale[0] = width;
    }

    getWidth () {
        return this._scale[0];
    }

    increaseWidthBy (delta) {
        this._scale[0] += delta;
    }

    setHeight (height) {
        this._scale[1] = height;
    }

    getHeight () {
        return this._scale[1];
    }

    increaseHeightBy (delta) {
        this._scale[1] += delta;
    }

    setRotationInRadian (rotationInRadian) {
        this._rotationInRadian = rotationInRadian;
        while (this._rotationInRadian > (2*Math.PI)) {
            this._rotationInRadian -= (2*Math.PI);
        }
    }

    getRotationInRadian () {
        return this._rotationInRadian;
    }

    increaseRotationByRadian (delta) {
        this.setRotationInRadian(this._rotationInRadian + delta);
    }

    setRotationInDegree (rotationInDegree) {
        this.setRotationInRadian(rotationInDegree * Math.PI/180.0);
    }

    getRotationInDegree () {
        return this._rotationInRadian * 180 / Math.PI;
    }

    increaseRotationByDegree (delta) {
        this.increaseRotationByRadian(delta * Math.PI / 180.0);
    }
}
