import gEngine from "../../GameEngine.js";
import { PerlinNoise } from "../../GameEngine.js";

export default class NoiseMap {
    constructor (width, height, scale, octaves, persistence, lacunarity, seed, offset) {
        this._width = width;
        this._height = height;
        this._octaves = octaves;
        this._persistence = persistence;
        this._lacunarity = lacunarity;
        this._seed = seed;
        this._offset = offset || vec2.fromValues(0, 0);

        this.scale = scale;

        this._seedMap(seed);

        this.update();
    }

    get width () {
        return this._width;
    }

    set width (value) {
        this._width = value;
        this._setMapSize();
    }

    get height () {
        return this._height;
    }

    set height (value) {
        this._height = value;
        this._setMapSize();
    }

    get octaves () {
        return this._octaves;
    }

    set octaves (value) {
        value = Math.max(0, value);
        this._octaves = value;
        this._setupOctaves();
    }

    get persistence () {
        return this._persistence;
    }

    set persistence (value) {
        this._persistence = value;
    }

    get lacunarity () {
        return this._lacunarity;
    }

    set lacunarity (value) {
        this._lacunarity = value;
    }

    get seed () {
        return this._seed;
    }

    set seed (value) {
        this._seedMap(value);
    }

    get lacunarity () {
        return this._lacunarity;
    }

    set lacunarity (value) {
        this._lacunarity = value;
    }

    get offset () {
        return this._offset;
    }

    set offset (value) {
        this._offset = value;
        this._setupOctaves();
    }

    increaseOffsetX (delta) {
        this._offset[0] += delta;
        this._setupOctaves();
    }

    increaseOffsetY (delta) {
        this._offset[1] += delta;
        this._setupOctaves();
    }

    get scale () {
        return this._scale;
    }

    set scale (value) {
        if (value <= 0) {
            value = 0.0001;
        }

        this._scale = value;
    }

    _seedMap (seed) {
        this._seed = seed;
        this._noise = new PerlinNoise(seed);
        this._randomOffsetX = gEngine.Random.randomFloat(-100000, 100000);
        this._randomOffsetY = gEngine.Random.randomFloat(-100000, 100000);

        this._setMapSize();
        this._setupOctaves();
    }

    _setMapSize () {
        this._map = new Array(this._width * this._height);
    }

    _setupOctaves () {
        this._octaveOffsets = new Array(this._octaves);
        for (let i = 0; i < this._octaves; i++) {
            let offsetX = this._randomOffsetX + this._offset[0];
            let offsetY = this._randomOffsetY + this._offset[1];
            this._octaveOffsets[i] = vec2.fromValues(offsetX, offsetY);
        }
    }

    update () {
        let minNoiseHeight = Number.MAX_VALUE;
        let maxNoiseHeight = Number.MIN_VALUE;

        this.loopThroughMap((i, x, y) => {
            let amplitude = 1;
            let frequency = 1;
            let noiseHeight = 0;

            for (let octave = 0; octave < this._octaves; octave++) {
                let sampleX = x / this._scale * frequency + this._octaveOffsets[octave][0];
                let sampleY = y / this._scale * frequency + this._octaveOffsets[octave][1];
                let perlinValue = this._noise.getValue(sampleX, sampleY, 0);
                noiseHeight += perlinValue * amplitude;

                amplitude *= this._persistence;
                frequency *= this._lacunarity;
            }

            if (noiseHeight > maxNoiseHeight) {
                maxNoiseHeight = noiseHeight;
            } else if (noiseHeight < minNoiseHeight) {
                minNoiseHeight = noiseHeight;
            }
            this._map[i] = noiseHeight;
        });

        this.loopThroughMap((i, x, y) => {
            this._map[i] = Math.inverseLerp(minNoiseHeight, maxNoiseHeight, this._map[i]);
        });
    }

    loopThroughMap (callback) {
        for (let i = 0; i < this._width * this._height; i++) {
            let x = i % this._width;
            let y = Math.floor(i / this._width);
            callback(i, x, y);
        }
    }

    getHeight () {
        return this._height;
    }

    getWidth () {
        return this._width;
    }

    getArray () {
        return this._map;
    }
}
