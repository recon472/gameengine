export default class CollisionInfo {
    constructor () {
        this._depth = 0;
        this._normal = vec2.fromValues(0, 0);
    }

    getDepth () {
        return this._depth;
    }

    setDepth (depth) {
        this._depth = depth;
    }

    getNormal () {
        return this._normal;
    }

    setNormal (normal) {
        this._normal = normal;
    }
}
