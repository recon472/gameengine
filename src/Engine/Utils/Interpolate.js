export default class Interpolate {
    constructor (value, cycles, rate) {
        this._currentValue = value;
        this._finalValue = value;
        this._cycles = cycles;
        this._rate = rate;

        this._cyclesLeft = 0;
    }

    _interpolateValue () {
        this._currentValue = this._currentValue + this._rate * (this._finalValue - this._currentValue);
    }

    getValue () {
        return this._currentValue;
    }

    configureInterpolation (stiffness, duration) {
        this._rate = stiffness;
        this._cycles = duration;
    }

    setFinalValue (value) {
        this._finalValue = value;
        this._cyclesLeft = this._cycles;
    }

    updateInterpolation () {
        if (this._cyclesLeft <= 0) {
            return;
        }
        this._cyclesLeft--;
        if (this._cyclesLeft === 0) {
            this._currentValue = this._finalValue;
        } else {
            this._interpolateValue();
        }
    }
}
