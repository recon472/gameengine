export default class ShakePosition {
    constructor (xMagnitude, yMagnitude, shakeFrequency, shakeDuration) {
        this._xMagnitude = xMagnitude;
        this._yMagnitude = yMagnitude;
        this._cycles = shakeDuration;
        this._omega = shakeFrequency * 2 * Math.PI;
        this._numberOfCyclesLeft = shakeDuration;
    }

    _nextDampedHarmonic () {
        const fraction = this._numberOfCyclesLeft / this._cycles;
        return fraction * fraction * Math.cos((1 - fraction) * this._omega);
    }

    isShakeDone () {
        return (this._numberOfCyclesLeft <= 0);
    }

    getShakeResults () {
        this._numberOfCyclesLeft--;
        let center = [];
        let fx = 0;
        let fy = 0;
        if (!this.isShakeDone()) {
            let value = this._nextDampedHarmonic();
            fx = (Math.random() > 0.5) ? -value : value;
            fy = (Math.random() > 0.5) ? -value : value;
        }
        center[0] = this._xMagnitude * fx;
        center[1] = this._yMagnitude * fy;
        return center;
    }
}
