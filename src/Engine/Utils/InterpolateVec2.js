import { Interpolate } from "../../GameEngine.js";

export default class InterpolateVec2 extends Interpolate {
    constructor (value, cycle, rate) {
        super(value, cycle, rate);
    }

    _interpolateValue () {
        vec2.lerp(this._currentValue, this._currentValue, this._finalValue, this._rate);
    }
}
