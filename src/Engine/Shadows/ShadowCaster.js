import gEngine from "../../GameEngine.js";
import {
    Transform, Light, SpriteRenderable
 } from "../../GameEngine.js";

export default class ShadowCaster {
    constructor (shadowCaster, shadowReceiver) {
        this._shadowCaster = shadowCaster;
        this._shadowReceiver = shadowReceiver;
        this._casterShader = gEngine.DefaultResources.getShadowCasterShader();
        this._shadowColor = [0, 0, 0, 0.2];
        this._saveTransform = new Transform();

        this.kCasterMaxScale = 3;
        this.kVerySmall = 0.001;
        this.kDistanceFudge = 0.01;
        this.kReceiverDistanceFudge = 0.6;
    }

    setShadowColor (color) {
        this._shadowColor = color;
    }

    draw (camera) {
        let casterRenderable = this._shadowCaster.getRenderable();
        this._shadowCaster.getTransform().cloneTo(this._saveTransform);
        let shader = casterRenderable.swapShader(this._casterShader);
        let color = casterRenderable.getColor();
        casterRenderable.setColor(this._shadowColor);
        let light;
        for (let i = 0; i < casterRenderable.numberOfLights(); i++) {
            light = casterRenderable.getLightAt(i);
            if (light.isLightOn() && light.isLightCastingShadow()) {
                this._saveTransform.cloneTo(this._shadowCaster.getTransform());
                if (this._computeShadowGeometry(light)) {
                    this._casterShader.setLight(light);
                    SpriteRenderable.prototype.draw.call(casterRenderable, camera);
                }
            }
        }
        this._saveTransform.cloneTo(this._shadowCaster.getTransform());
        casterRenderable.swapShader(shader);
        casterRenderable.setColor(color);
    }

    _computeShadowGeometry (light) {
        let casterTransform = this._shadowCaster.getTransform();
        let receiverTransform = this._shadowReceiver.getTransform();
        let lightToCaster = vec3.create();
        let lightToReceiverZ;
        let receiverToCasterZ;
        let distanceToCaster, distanceToReceiver;
        let scale;
        let offset = vec3.fromValues(0, 0, 0);

        receiverToCasterZ = receiverTransform.getZPosition() - casterTransform.getZPosition();
        if (light.getLightType() === Light.eLightType.eDirectionalLight) {
            if (((Math.abs(light.getDirection())[2]) < this.kVerySmall) ||
                ((receiverToCasterZ * (light.getDirection())[2]) < 0)) {
                return false;
            }
            vec3.copy(lightToCaster, light.getDirection());
            vec3.normalize(lightToCaster, lightToCaster);

            distanceToReceiver = Math.abs(receiverToCasterZ / lightToCaster[2]);
            scale = Math.abs(1 / lightToCaster[2]);
        } else {
            vec3.sub(lightToCaster, casterTransform.get3DPosition(), light.getPosition());
            lightToReceiverZ = receiverTransform.getZPosition() - (light.getPosition())[2];

            if ((lightToReceiverZ * lightToCaster[2]) < 0) {
                return false;
            }

            if ((Math.abs(lightToReceiverZ) < this.kVerySmall) ||
                ((Math.abs(lightToCaster[2]) < this.kVerySmall))) {
                return false;
            }

            distanceToCaster = vec3.length(lightToCaster);
            vec3.scale(lightToCaster, lightToCaster, 1 / distanceToCaster);

            distanceToReceiver = Math.abs(receiverToCasterZ / lightToCaster[2]);
            scale = (distanceToCaster + (distanceToReceiver * this.kReceiverDistanceFudge)) / distanceToCaster;
        }

        vec3.scaleAndAdd(offset, casterTransform.get3DPosition(), lightToCaster, distanceToReceiver + this.kDistanceFudge);

        casterTransform.setRotationInRadian(casterTransform.getRotationInRadian());
        casterTransform.setPosition(offset[0], offset[1]);
        casterTransform.setZPosition(offset[2]);
        casterTransform.setWidth(casterTransform.getWidth() * scale);
        casterTransform.setHeight(casterTransform.getHeight() * scale);

        return true;
    }
}
