import gEngine from "../../GameEngine.js";
import {
    GameComponent, ShadowCaster
} from "../../GameEngine.js";

export default class ShadowReceiver extends GameComponent {
    constructor (theReceiverObject) {
        super();
        this._receiveShadows = false;
        this.kShadowStencilBit = 0x01;
        this.kShadowStencilMask = 0xFF;
        this._receiverShader = gEngine.DefaultResources.getShadowReceiverShader();

        this._receiver = theReceiverObject;

        this._shadowCaster = [];
    }

    addShadowCaster (lightRenderable) {
        const caster = new ShadowCaster(lightRenderable, this._receiver);
        this._shadowCaster.push(caster);
    }

    draw (camera) {
        this._receiver.getRenderable().draw(camera);

        this._shadowReceiverStencilOn();
        const shader = this._receiver.getRenderable().swapShader(this._receiverShader);
        this._receiver.getRenderable().draw(camera);
        this._receiver.getRenderable().swapShader(shader);
        this._shadowReceiverStencilOff();

        for (let i = 0; i < this._shadowCaster.length; i++) {
            this._shadowCaster[i].draw(camera);
        }

        this._shadowReceiverStencilDisable();
    }

    getReceiveShadows () {
        return this._receiveShadows;
    }

    setReceiveShadows (value) {
        this._receiveShadows = value;
    }
}
