import { GameComponent, BoundingBox } from "../../GameEngine.js";

export default class HitCollisionComponent extends GameComponent {
    getBoundingBox () {
        let transform = this.gameObject.getTransform();
        let boundingBox = new BoundingBox(
            transform.getPosition(),
            transform.getWidth(),
            transform.getHeight()
        );
        return boundingBox;
    }

    pixelTouches (otherObject, wcTouchPosition = vec2.create()) {
        let pixelTouch = false;
        let myRenderable = this.gameObject.getRenderable();
        let otherRenderable = otherObject.getRenderable();

        if ((myRenderable.getTransform().getRotationInRadian() === 0) &&
            (otherRenderable.getTransform().getRotationInRadian() === 0)) {
            let otherBoundingBox = otherObject.getComponent("HitCollisionComponent").getBoundingBox();
            if (otherBoundingBox.intersectsBound(this.getBoundingBox())) {
                myRenderable.setColorArray();
                otherRenderable.setColorArray();
                pixelTouch = myRenderable.pixelTouches(otherRenderable, wcTouchPosition);
            }
        } else {
            // one or both is rotated, use a encompasing circle, hypertenuse as radius
            let mySize = myRenderable.getTransform().getSize();
            let otherSize = otherRenderable.getTransform().getSize();
            let myRadius = Math.sqrt(0.5 * mySize[0] * 0.5 * mySize[0] + 0.5 * mySize[1] * 0.5 * mySize[1]);
            let otherRadius = Math.sqrt(0.5 * otherSize[0] * 0.5 * otherSize[0] + 0.5 * otherSize[1] * 0.5 * otherSize[1]);

            let decomposedVector = [];
            vec2.sub(decomposedVector, myRenderable.getTransform().getPosition(), otherRenderable.getTransform().getPosition());
            if (vec2.length(decomposedVector) < (myRadius + otherRadius)) {
                myRenderable.setColorArray();
                otherRenderable.setColorArray();
                pixelTouch = myRenderable.pixelTouches(otherRenderable, wcTouchPosition);
            }
        }
        return pixelTouch;
    }
}
