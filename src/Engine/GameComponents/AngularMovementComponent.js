import { GameComponent } from "../../GameEngine.js";

export default class AngularMovementComponent extends GameComponent {
    constructor () {
        super();
        this._currentFrontDirection = vec2.fromValues(0, 1);
        this._speed = 0;
        this._targetTransform = null;
        this._angularSpeed = 0;
    }

    update () {
        super.update();
        if (this._targetTransform !== null) {
            this.rotateObjectToPointTo(this._targetTransform.getPosition(), this._angularSpeed);
        }
        let position = this.gameObject.getTransform().getPosition();
        vec2.scaleAndAdd(position, position, this.getCurrentFrontDirection(), this.getSpeed());
    }

    setTargetTransform (transform) {
        this._targetTransform = transform;
    }

    setSpeed (speed) {
        this._speed = speed;
    }

    getSpeed () {
        return this._speed;
    }

    setAngularSpeed (speed) {
        this._angularSpeed = speed;
    }

    increaseAngularSpeedBy (delta) {
        this._angularSpeed += speed;
    }

    getAngularSpeed () {
        return this._angularSpeed;
    }

    increaseSpeedBy (delta) {
        this._speed += delta;
    }

    setCurrentFrontDirection (direction) {
        vec2.normalize(this._currentFrontDirection, direction);
    }

    getCurrentFrontDirection () {
        return this._currentFrontDirection;
    }

    rotateObjectToPointTo (position, rate) {
        // determine if we've reached the position
        let direction = [];
        vec2.sub(direction, position, this.gameObject.getTransform().getPosition());
        let length = vec2.length(direction);
        if (length < Number.MIN_VALUE) {
            return; // we are there
        }
        vec2.scale(direction, direction, 1 / length);

        // compute the angle to rotate
        let frontDirection = this.getCurrentFrontDirection();
        let cosTheta = vec2.dot(direction, frontDirection);
        if (cosTheta > 0.999999) { // almost the same direction
            return;
        }

        // clamp the angle to -1 to 1 just to be sure
        if (cosTheta > 1) {
            cosTheta = 1;
        } else {
            if (cosTheta < -1) {
                cosTheta = -1;
            }
        }

        // compute whether to rotate clockwise or counterclockwise
        let direction3D = vec3.fromValues(direction[0], direction[1], 0);
        let front3D = vec3.fromValues(frontDirection[0], frontDirection[1], 0);
        let rotation3D = [];
        vec3.cross(rotation3D, front3D, direction3D);

        let radians = Math.acos(cosTheta); // radians to rotate
        if (rotation3D[2] < 0) {
            radians = -radians;
        }

        // rotate the facing direction with the angle and rate
        radians *= rate;
        vec2.rotate(this.getCurrentFrontDirection(), this.getCurrentFrontDirection(), radians);
        this.gameObject.getTransform().increaseRotationByRadian(radians);
    }

    rotateByRadianWithTransform (radian) {
        this.gameObject.getTransform().increaseRotationByRadian(radian);
        vec2.rotate(this.getCurrentFrontDirection(), this.getCurrentFrontDirection(), radian);
    }
}
