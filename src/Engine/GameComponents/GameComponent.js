import { NetworkObject } from "../../GameEngine.js";
// all subclassess MUST be named ****Component.js

// game component and it's subclass mustn't have any constructor parameters,
export default class GameComponent extends NetworkObject {
    constructor () {
        super();
        this.gameObject = null;
    }
    // reference to other components must be set in this function only
    initialize () { }

    // this function should be subclassed only by renderable
    draw (camera) { }

    // update loop for game logic
    update () {
        super.update();
    }
}
