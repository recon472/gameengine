import gEngine from "../../GameEngine.js";
import { LineRenderable } from "../../GameEngine.js";

export default class Particle {
    constructor (position) {
        this.kPadding = 0.5;

        this._position = position;
        this._velocity = vec2.fromValues(0, 0);
        this._acceleration = gEngine.Particles.getSystemAcceleration();
        this._drag = 0.95;

        this._positionMark = new LineRenderable();
        this._drawBounds = false;
    }

    draw (camera) {
        if (!this._drawBounds) {
            return;
        }

        let x = this._position[0];
        let y = this._position[1];

        this._positionMark.setFirstVertex(x - this.kPadding, y + this.kPadding);
        this._positionMark.setSecondVertex(x + this.kPadding, y - this.kPadding);
        this._positionMark.draw(camera);

        this._positionMark.setFirstVertex(x + this.kPadding, y + this.kPadding);
        this._positionMark.setSecondVertex(x - this.kPadding, y - this.kPadding);
        this._positionMark.draw(camera);
    }

    update () {
        let deltaTime = gEngine.GameLoop.getUpdateIntervalInSeconds();
        let position = this.getPosition();
        vec2.scaleAndAdd(this._velocity, this._velocity, this._acceleration, deltaTime);
        vec2.scale(this._velocity, this._velocity, this._drag);
        vec2.scaleAndAdd(position, position, this._velocity, deltaTime);
    }

    getPosition () {
        return this._position;
    }

    setPosition (position) {
        this._position = position;
    }

    getVelocity () {
        return this._velocity;
    }

    setVelocity (velocity) {
        this._velocity = velocity;
    }

    getAcceleration () {
        return this._acceleration;
    }

    setAcceleration (acceleration) {
        this._acceleration = acceleration;
    }

    getDrag () {
        return this._drag;
    }

    setDrag (drag) {
        this._drag = drag;
    }

    getDrawBounds () {
        return this._drawBounds;
    }

    setDrawBounds (draw) {
        this._drawBounds = draw;
    }

    getColor () {
        return this._positionMark.getColor();
    }

    setColor (color) {
        this._positionMark.setColor(color);
    }
}
