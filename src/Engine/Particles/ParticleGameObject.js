import { GameObject, ParticleRenderable, Particle } from "../../GameEngine.js";

export default class ParticleGameObject extends GameObject {
    constructor (texture, atX, atY, cyclesToLive, zValue) {
        let renderable = new ParticleRenderable(texture);
        let transform = renderable.getTransform();
        transform.setPosition(atX, atY);
        super(renderable, zValue);

        let particle = new Particle(transform.getPosition());
        this.setPhysics(particle);

        this._deltaColor = [0, 0, 0, 0];
        this._sizeDelta = 0;
        this._cyclesToLive = cyclesToLive;
    }

    setFinalColor (color) {
        vec4.sub(this._deltaColor, color, this.getRenderable().getColor());
        if (this._cyclesToLive !== 0) {
            vec4.scale(this._deltaColor, this._deltaColor, 1 / this._cyclesToLive);
        }
    }

    setSizeDelta (delta) {
        this._sizeDelta = delta;
    }

    hasExpired () {
        return (this._cyclesToLive < 0);
    }

    update () {
        super.update();

        this._cyclesToLive--;
        let color = this.getRenderable().getColor();
        vec4.add(color, color, this._deltaColor);

        let transform = this.getTransform();
        let size = transform.getWidth() * this._sizeDelta;
        transform.setSize(size, size);
    }
}
