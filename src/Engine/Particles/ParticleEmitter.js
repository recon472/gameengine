import gEngine from "../../GameEngine.js";
import { GameObject } from "../../GameEngine.js";

export default class ParticleEmitter extends GameObject {
    constructor (position, emitLength, emitsPerSecond, emitsInBatch, zValue, creatorFunction) {
        super(null, zValue);

        this._startTime = gEngine.Time.getElapsedTime();
        this._lastEmit = 0;

        this._position = position;
        this._emitLength = emitLength * 1000;
        this._emitsPerSecond = 1 / emitsPerSecond * 1000;
        this._emitsInBatch = emitsInBatch;
        this._creatorFunction = creatorFunction;

        this._particles = [];
    }

    getParticleAt (index) {
        return this._particles[index];
    }

    getParticleCount () {
        return this._particles.length;
    }

    draw (camera) {
        let gl = gEngine.Core.getGL();
        gl.blendFunc(gl.ONE, gl.ONE);
        for (let i = 0; i < this._particles.length; i++) {
            this._particles[i].draw(camera);
        }
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    }

    update () {
        let elapsedTime = gEngine.Time.getElapsedTime();
        if (this._emitLength === 0 || this._emitLength > elapsedTime - this._startTime) {
            if (this._emitsPerSecond < elapsedTime - this._lastEmit) {
                this._lastEmit = elapsedTime;
                for (let i = 0; i < this._emitsInBatch; i++) {
                    this._particles.push(this._creatorFunction(this._position[0], this._position[1]));
                }
            }
        } else {
            if (this._particles.length < 1) {
                this.destroy();
            }
        }

        for (let i = this._particles.length - 1; i >= 0; i--) {
            let particle = this._particles[i];
            particle.update();
            if (particle.hasExpired()) {
                this._particles.splice(i, 1);
            }
        }
    }
}
