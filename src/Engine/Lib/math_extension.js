Math.inverseLerp = function (a, b, value) {
    var min = Math.min(a, b);
    var max = Math.max(a, b);

    if (value < min || value > max) {
        return 0;
    }

    var value = (value - min) / (max - min);
    return value;
};

Math.lerp = function (a, b, amount) {
	amount = amount < 0 ? 0 : amount;
	amount = amount > 1 ? 1 : amount;
	return a + (b - a) * amount;
};

Math.clamp = function (value, min, max) {
    return Math.min(Math.max(value, min), max);
};
