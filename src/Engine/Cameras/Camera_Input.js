import gEngine from "../../GameEngine.js";
import { Camera } from "../../GameEngine.js";

export default null;

Camera.prototype._mouseDCX = function () {
    return gEngine.Input.getMousePositionX() - this._viewport[Camera.eViewport.eOriginX];
};

Camera.prototype._mouseDCY = function () {
    return gEngine.Input.getMousePositionY() - this._viewport[Camera.eViewport.eOriginY];
};

Camera.prototype._touchDCX = function () {
    return gEngine.Input.getTouchPositionX() - this._viewport[Camera.eViewport.eOriginX];
};

Camera.prototype._touchDCY = function () {
    return gEngine.Input.getTouchPositionY() - this._viewport[Camera.eViewport.eOriginY];
};

Camera.prototype.isMouseInViewport = function () {
    const dcX = this._mouseDCX();
    const dcY = this._mouseDCY();
    return ((dcX >= 0) && (dcX < this._viewport[Camera.eViewport.eWidth]) &&
            (dcY >= 0) && (dcY < this._viewport[Camera.eViewport.eHeight]));
};

Camera.prototype.isTouchInViewport = function () {
    const dcX = this._touchDCX();
    const dcY = this._touchDCY();
    return ((dcX >= 0) && (dcX < this._viewport[Camera.eViewport.eWidth]) &&
            (dcY >= 0) && (dcY < this._viewport[Camera.eViewport.eHeight]));
};

Camera.prototype.mouseWCX = function () {
    const minWCX = this.getWCCenter()[0] - this.getWCWidth() / 2;
    return minWCX + (this._mouseDCX()) * (this.getWCWidth() / this._viewport[Camera.eViewport.eWidth]);
};

Camera.prototype.mouseWCY = function () {
    const minWCY = this.getWCCenter()[1] - this.getWCHeight() / 2;
    return minWCY + (this._mouseDCY()) * (this.getWCHeight() / this._viewport[Camera.eViewport.eHeight]);
};

Camera.prototype.touchWCX = function () {
    const minWCX = this.getWCCenter()[0] - this.getWCWidth() / 2;
    return minWCX + (this._touchDCX()) * (this.getWCWidth() / this._viewport[Camera.eViewport.eWidth]);
};

Camera.prototype.touchWCY = function () {
    const minWCY = this.getWCCenter()[1] - this.getWCHeight() / 2;
    return minWCY + (this._touchDCY()) * (this.getWCHeight() / this._viewport[Camera.eViewport.eHeight]);
};
