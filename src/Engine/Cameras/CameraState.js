import {
    Interpolate, InterpolateVec2
} from "../../GameEngine.js";

export default class CameraState {
    constructor (center, width) {
        this.kCycles = 300;
        this.kRate = 0.1;
        this._center = new InterpolateVec2(center, this.kCycles, this.kRate);
        this._width = new Interpolate(width, this.kCycles, this.kRate);
    }

    getCenter () {
        return this._center.getValue();
    }

    getWidth () {
        return this._width.getValue();
    }

    setCenter (center) {
        this._center.setFinalValue(center);
    }

    setWidth (width) {
        this._width.setFinalValue(width);
    }

    updateCameraState () {
        this._center.updateInterpolation();
        this._width.updateInterpolation();
    }

    configureInterpolation (stiffness, duration) {
        this._center.configureInterpolation(stiffness, duration);
        this._width.configureInterpolation(stiffness, duration);
    }
}
