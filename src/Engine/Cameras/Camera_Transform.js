import { Camera } from "../../GameEngine.js";

export default null;

Camera.prototype.fakeZInPixelSpace = function (z) {
    return z * this._renderCache._wCToPixelRatio;
};

Camera.prototype.wcPositionToPixel = function (position) {
    const x = this._viewport[Camera.eViewport.eOriginX] + ((position[0] - this._renderCache._cameraOriginX) * this._renderCache._wCToPixelRatio) + 0.5;
    const y = this._viewport[Camera.eViewport.eOriginY] + ((position[1] - this._renderCache._cameraOriginY) * this._renderCache._wCToPixelRatio) + 0.5;
    const z = this.fakeZInPixelSpace(position[2]);
    return vec3.fromValues(x, y, z);
};

Camera.prototype.wcSizeToPixel = function (size) {
    return (size * this._renderCache._wCToPixelRatio) + 0.5;
};
