import gEngine from "../../GameEngine.js";
import {
    CameraState, BoundingBox
 } from "../../GameEngine.js";

class PerRenderCache {
    constructor () {
        this._wCToPixelRatio = 1;
        this._cameraOriginX = 1;
        this._cameraOriginY = 1;
        this._cameraPositionInPixelSpace = vec3.fromValues(0, 0, 0);
    }
}

export default class Camera {
    constructor (wcCenter, wcWidth, viewportArray, bound, cameraZ) {
        this.kId = gEngine.Core.getNewId();

        this.gameWorld = null;

        this._cameraState = new CameraState(wcCenter, wcWidth);
        this._cameraShake = null;

        this._viewport = []; // [x, y, width, height]
        this._viewportBound = 0;
        if (bound !== undefined) {
            this._viewportBound = bound;
        }
        this._scissorBound = [];
        this.setViewport(viewportArray, this._viewportBound);

        this._nearPlane = 0;
        this._farPlane = 1000;
        this.kCameraZ = cameraZ || 10;

        this._viewMatrix = mat4.create();
        this._projectionMatrix = mat4.create();
        this._viewportMatrix = mat4.create();

        this._backgroundColor = [102/255, 204/255, 255/255, 1];

        this._renderCache = new PerRenderCache();
    }

    clampAtBoundary (transform, zone) {
        let status = this.collideWCBound(transform, zone);
        if (status !== BoundingBox.eBoundCollideStatus.eInside) {
            let position = transform.getPosition();
            if ((status & BoundingBox.eBoundCollideStatus.eCollideTop) !== 0) {
                position[1] = (this.getWCCenter())[1] + (zone * this.getWCHeight() / 2) - (transform.getHeight() / 2);
            }
            if ((status & BoundingBox.eBoundCollideStatus.eCollideBottom) !== 0) {
                position[1] = (this.getWCCenter())[1] - (zone * this.getWCHeight() / 2) + (transform.getHeight() / 2);
            }
            if ((status & BoundingBox.eBoundCollideStatus.eCollideRight) !== 0) {
                position[0] = (this.getWCCenter())[0] + (zone * this.getWCWidth() / 2) - (transform.getWidth() / 2);
            }
            if ((status & BoundingBox.eBoundCollideStatus.eCollideLeft) !== 0) {
                position[0] = (this.getWCCenter())[0] - (zone * this.getWCWidth() / 2) + (transform.getWidth() / 2);
            }
        }
        return status;
    }

    setupViewProjection () {
        const gl = gEngine.Core.getGL();

        // configure viewport
        gl.viewport(this._viewport[0],
            this._viewport[1],
            this._viewport[2],
            this._viewport[3]);

        gl.scissor(this._scissorBound[0],
            this._scissorBound[1],
            this._scissorBound[2],
            this._scissorBound[3]);

        gl.clearColor(this._backgroundColor[0],
            this._backgroundColor[1],
            this._backgroundColor[2],
            this._backgroundColor[3]);

        gl.enable(gl.SCISSOR_TEST);
        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.disable(gl.SCISSOR_TEST);

        // get proper center
        let center = [];
        if (this._cameraShake !== null) {
            center = this._cameraShake.getCenter();
        } else {
            center = this.getWCCenter();
        }

        // define View-Projection matrix
        mat4.lookAt(this._viewMatrix,
            [center[0], center[1], this.kCameraZ],
            [center[0], center[1], 0],
            [0, 1, 0]);

        const halfWCWidth = 0.5 * this.getWCWidth();
        const halfWCHeight = 0.5 * this.getWCHeight();

        mat4.ortho(this._projectionMatrix,
            -halfWCWidth,
            halfWCWidth,
            -halfWCHeight,
            halfWCHeight,
            this._nearPlane,
            this._farPlane);

        mat4.multiply(this._viewportMatrix, this._projectionMatrix, this._viewMatrix);

        // compute render cache for efficient wcToPixel conversions
        this._renderCache._wCToPixelRatio = this._viewport[Camera.eViewport.eWidth] / this.getWCWidth();
        this._renderCache._cameraOriginX = center[0] - (this.getWCWidth() / 2);
        this._renderCache._cameraOriginY = center[1] - (this.getWCHeight() / 2);

        // compute position for material rendering
        const position = this.wcPositionToPixel(this.getWCCenter());
        this._renderCache._cameraPositionInPixelSpace[0] = position[0];
        this._renderCache._cameraPositionInPixelSpace[1] = position[1];
        this._renderCache._cameraPositionInPixelSpace[2] = this.fakeZInPixelSpace(this.kCameraZ);
    }

    collideWCBound (transform, zone) {
        const boundingBox = new BoundingBox(
            transform.getPosition(),
            transform.getWidth(),
            transform.getHeight()
        );
        const width = zone * this.getWCWidth();
        const height = zone * this.getWCHeight();
        const cameraBoundingBox = new BoundingBox(this.getWCCenter(), width, height);
        return cameraBoundingBox.boundCollideStatus(boundingBox);
    }

    wcDirectionToPixel (direction) {
        const x = direction[0] * this._renderCache._wCToPixelRatio;
        const y = direction[1] * this._renderCache._wCToPixelRatio;
        const z = direction[2];
        return vec3.fromValues(x, y, z);
    }

    getPositionInPixelSpace () {
        return this._renderCache._cameraPositionInPixelSpace;
    }

    setWCCenter (xPosition, yPosition) {
        this._cameraState.setCenter(vec2.fromValues(xPosition, yPosition));
    }

    getWCCenter () {
        return this._cameraState.getCenter();
    }

    setWCWidth (width) {
        this._cameraState.setWidth(width);
    }

    getWCWidth () {
        return this._cameraState.getWidth();
    }

    getWCHeight () {
        return this._cameraState.getWidth() * this._viewport[3] / this._viewport[2];
    }

    setViewport (viewportArray, bound) {
        if (bound === undefined) {
            bound = this._viewportBound;
        }

        this._viewport[0] = viewportArray[0] + bound;
        this._viewport[1] = viewportArray[1] + bound;
        this._viewport[2] = viewportArray[2] - (2 * bound);
        this._viewport[3] = viewportArray[3] - (2 * bound);
        this._scissorBound[0] = viewportArray[0];
        this._scissorBound[1] = viewportArray[1];
        this._scissorBound[2] = viewportArray[2];
        this._scissorBound[3] = viewportArray[3];
    }

    getViewport () {
        let out = [];
        out[0] = this._scissorBound[0];
        out[1] = this._scissorBound[1];
        out[2] = this._scissorBound[2];
        out[3] = this._scissorBound[3];
        return out;
    }

    setBackgroundColor (newColor) {
        this._backgroundColor = newColor;
    }

    getBackgroundColor () {
        return this._backgroundColor;
    }

    getViewportMatrix () {
        return this._viewportMatrix;
    }

    getObjectsAtMousePosition () {
        if (this.gameWorld === null) {
            return null;
        }
        let x = this.mouseWCX();
        let y = this.mouseWCY();
        let box = new BoundingBox(vec2.fromValues(x, y), 1, 1);
        let objects = [];
        let goDictionary = this.gameWorld.getGameObjects();
        for (let key in goDictionary) {
            let go = goDictionary[key];
            let goBox = go.getComponent("HitCollisionComponent").getBoundingBox();
            if (goBox.boundCollideStatus(box) !== BoundingBox.eBoundCollideStatus.eOutside) {
                objects.push(go);
            }
        }
        return objects;
    }

    getTopObjectAtMousePosition () {
        if (this.gameWorld === null) {
            return null;
        }
        let objects = this.getObjectsAtMousePosition();
        let result = null;
        let zValue = Number.MAX_VALUE;
        for (let i = 0; i < objects.length; i++) {
            let object = objects[i];
            if (object.kZ < zValue) {
                zValue = object.kZ;
                result = object;
            }
        }
        return result;
    }
}

Camera.eViewport = Object.freeze({
    eOriginX: 0,
    eOriginY: 1,
    eWidth: 2,
    eHeight: 3
});
