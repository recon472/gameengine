export default class CameraShake {
    constructor (state, xMagnitude, yMagnitude, shakeFrequency, shakeDuration) {
        this._originCenter = vec2.clone(state.getCenter());
        this._shakeCenter = vec2.clone(this._originCenter);
        this._shake = new ShakePosition(xMagnitude, yMagnitude, shakeFrequency, shakeDuration);
    }

    updateShakeState  () {
        const shake = this._shake.getShakeResults();
        vec2.add(this._shakeCenter, this._originCenter, shake);
    }

    isShakeDone  () {
        return this._shake.isShakeDone();
    }

    getCenter  () {
        return this._shakeCenter;
    }

    setReferenceCenter  (center) {
        this._originCenter[0] = center[0];
        this._originCenter[1] = center[1];
    }
}
