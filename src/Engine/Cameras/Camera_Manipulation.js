import {
    Camera, BoundingBox
 } from "../../GameEngine.js";

export default null;

Camera.prototype.update = function () {
    if (this._cameraShake !== null) {
        if (this._cameraShake.isShakeDone()) {
            this._cameraShake = null;
        } else {
            this._cameraShake.setReferenceCenter(this.getWCCenter());
            this._cameraShake.updateShakeState();
        }
    }
    this._cameraState.updateCameraState();
};

Camera.prototype.shake = function (xMagnitude, yMagnitude, shakeFrequency, duration) {
    this._cameraShake = new CameraShake(this._cameraState, xMagnitude, yMagnitude, shakeFrequency, duration);
};

Camera.prototype.panBy = function (deltaX, deltaY) {
    let newCenter = vec2.clone(this.getWCCenter());
    newCenter[0] += deltaX;
    newCenter[1] += deltaY;
    this._cameraState.setCenter(newCenter);
};

Camera.prototype.panTo = function (centerX, centerY) {
    this.setWCCenter(centerX, centerY);
};

Camera.prototype.panWith = function (transform, zone) {
    let status = this.collideWCBound(transform, zone);
    if (status !== BoundingBox.eBoundCollideStatus.eInside) {
        let position = transform.getPosition();
        let newCenter = vec2.clone(this.getWCCenter());
        if ((status & BoundingBox.eBoundCollideStatus.eCollideTop) !== 0) {
            newCenter[1] = position[1] + (transform.getHeight() / 2) - (zone * this.getWCHeight() / 2);
        }
        if ((status & BoundingBox.eBoundCollideStatus.eCollideBottom) !== 0) {
            newCenter[1] = position[1] - (transform.getHeight() / 2) + (zone * this.getWCHeight() / 2);
        }
        if ((status & BoundingBox.eBoundCollideStatus.eCollideRight) !== 0) {
            newCenter[0] = position[0] + (transform.getWidth() / 2) - (zone * this.getWCWidth() / 2);
        }
        if ((status & BoundingBox.eBoundCollideStatus.eCollideLeft) !== 0) {
            newCenter[0] = position[0] - (transform.getWidth() / 2) + (zone * this.getWCWidth() / 2);
        }
        this._cameraState.setCenter(newCenter);
    }
};

Camera.prototype.zoomBy = function (zoom) {
    if (zoom > 0) {
        this.setWCWidth(this.getWCWidth() * zoom);
    }
};

Camera.prototype.zoomTowards = function (position, zoom) {
    let delta = [];
    let newCenter = [];
    vec2.sub(delta, position, this.getWCCenter());
    vec2.scale(delta, delta, zoom - 1);
    vec2.sub(newCenter, this.getWCCenter(), delta);
    this.zoomBy(zoom);
    this._cameraState.setCenter(newCenter);
};

Camera.prototype.configureInterpolation = function (stiffness, duration) {
    this._cameraState.configureInterpolation(stiffness, duration);
};
