import gEngine from "../../GameEngine.js";
import { NetworkPacket } from "../../GameEngine.js";

export default class NetworkObject {
    constructor () {
        this._observedValues = [];
        this._valuesCache = {};
        this._lerpSpeed = 0.15;
        this._sendUpdatePackets = true;
        this._owner = -1;
        this._networkID = -1;
        this._triggers = [];
    }

    runFunctionOnNetwork (functionName, args = [], recipients = []) {
        let argsString = "";
        if ((args || []).length > 0) {
            argsString = '"' + args.join('","') + '"';
        }
        const trigger = {
            data: `this.${functionName}(${argsString});`,
            recipients: recipients
        };
        eval(trigger.data);
        this._triggers.push(trigger);
    }

    _evaluateRemoteTriggers (triggers) {
        for (let trigger of triggers) {
            eval(trigger.data);
        }
    }

    isSendingUpdatePackets () {
        return this._sendUpdatePackets;
    }

    setSendUpdatePackets (value) {
        this._sendUpdatePackets = value;
    }

    observeValue (name, smoothing = NetworkObject.eSmoothing.None) {
        this._observedValues.push({
            name: name,
            smoothing: smoothing
        });
    }

    getOwner () {
        return this._owner;
    }

    isOwner () {
        return this._owner === gEngine.Networking.getClientID() || this._owner === -1;
    }

    getNetworkID () {
        return this._networkID;
    }

    update () {
        for (let valueObject of this._observedValues) {
            if (this._valuesCache[valueObject.name] === undefined) {
                break;
            }
            switch (valueObject.smoothing) {
                case NetworkObject.eSmoothing.None:
                    this[valueObject.name] = this._valuesCache[valueObject.name];
                    break;
                case NetworkObject.eSmoothing.Interpolate:
                    const newValue = this._valuesCache[valueObject.name];
                    const oldValue = this[valueObject.name];

                    if (typeof newValue === "number") {
                        oldValue = Math.lerp(oldValue, newValue, this._lerpSpeed);
                        break;
                    }

                    for (let i in newValue) {
                        oldValue[i] = Math.lerp(oldValue[i], newValue[i], this._lerpSpeed);
                    }
                    break;
            }
        }
    }
}

NetworkObject.eSmoothing = Object.freeze({
    None: 0,
    Interpolate: 1
});
