import { GameObject } from "../../GameEngine.js";

export default class TiledGameObject extends GameObject {
    constructor (renderable, zValue) {
        super(renderable, zValue);
        this._shouldTile = true;
    }

    setIsTiled (value) {
        this._shouldTile = value;
    }

    shouldTile () {
        return this._shouldTile;
    }

    draw (camera) {
        if (this.isVisible()) {
            if (this.shouldTile()) {
                this._drawTile(camera);
            } else {
                this.getRenderable().draw(camera);
            }
        }
    }

    _drawTile (camera) {
        let transform = this.getTransform();
        let width = transform.getWidth();
        let height = transform.getHeight();
        let position = transform.getPosition();

        let left = position[0] - (width / 2);
        let right = left + width;
        let top = position[1] + (height / 2);
        let bottom = top - height;

        let wcPosition = camera.getWCCenter();
        let wcLeft = wcPosition[0] - (camera.getWCWidth() / 2);
        let wcRight = wcLeft + camera.getWCWidth();
        let wcBottom = wcPosition[1] - (camera.getWCHeight() / 2);
        let wcTop = wcBottom + camera.getWCHeight();

        let dx = 0, dy = 0;
        if (right < wcLeft) {
            dx = Math.ceil((wcLeft - right) / width) * width;
        } else {
            if (left > wcLeft) {
                dx = -Math.ceil((left - wcLeft) / width) * width
            }
        }
        if (top < wcBottom) {
            dy = Math.ceil((wcBottom - top) / height) * height;
        } else {
            if (bottom > wcBottom) {
                dy = -Math.ceil((bottom - wcBottom) / height) * height;
            }
        }

        let sx = position[0];
        let sy = position[1];
        transform.increaseXPositionBy(dx);
        transform.increaseYPositionBy(dy);
        right = position[0] + (width / 2);
        top = position[1] + (height / 2);

        let nx = 1;
        let ny = 1;
        nx = Math.ceil((wcRight - right) / width);
        ny = Math.ceil((wcTop - top) / height);

        let cx = nx;
        let xPosition = position[0];
        while (ny >= 0) {
            cx = nx;
            position[0] = xPosition;
            while (cx >= 0) {
                this.getRenderable().draw(camera);
                transform.increaseXPositionBy(width);
                cx--;
            }
            transform.increaseYPositionBy(height);
            ny--;
        }
        position[0] = sx;
        position[1] = sy;
    }
}
