import gEngine from "../../GameEngine.js";
import { ShadowReceiver, NetworkObject } from "../../GameEngine.js";

export default class GameObject extends NetworkObject {
    constructor (renderableObject = null, zValue = 10) {
        super();
        this._name = "GameObject";
        this._componentPool = {};
        this._physics = null;
        this._visible = true;
        this._initialized = false;

        this.gameWorld = null;
        this.kId = gEngine.Core.getNewId();
        this.kZ = zValue || 10;

        if (renderableObject !== null) {
            renderableObject.kId = gEngine.Core.getNewId();
            renderableObject.gameObject = this;
            this._componentPool["Renderable"] = renderableObject;
            this._componentPool["Transform"] = renderableObject.getTransform();
        }

        let shadowReceiver = new ShadowReceiver(this);
        shadowReceiver.kId = gEngine.Core.getNewId();
        shadowReceiver.gameObject = this;
        this._componentPool["ShadowReceiver"] = shadowReceiver;

        this.addComponent("HitCollisionComponent");
        this.addComponent("AngularMovementComponent");
    }

    transferOwnership (newOwnerID) {
        gEngine.Networking.transferObjectOwnership(this.getNetworkID(), newOwnerID);
    }

    // components

    addComponent (type) {
        const component = eval("new gEngine.ComponentManifest." + type + "()");
        component.gameObject = this;
        component.kId = gEngine.Core.getNewId();
        if (this._initialized) {
            component.initialize();
        }
        this._componentPool[type] = component;
    }

    removeComponent (type) {
        this._componentPool[type].gameObject = null;
        delete(this._componentPool[type]);
    }

    getComponent (type) {
        return this._componentPool[type];
    }

    setPhysics (physics) {
        this._physics = physics;
    }

    getPhysics () {
        return this._physics;
    }

    // update, draw, init

    initialize () {
        this._initialized = true;
        for (let component in this._componentPool) {
            this._componentPool[component].initialize();
        }
    }

    update () {
        super.update();
        for (let component in this._componentPool) {
            this._componentPool[component].update();
        }
        if (this._physics !== null) {
            this._physics.update();
        }
    }

    draw (camera) {
        if (this.isVisible()) {
            if (this._componentPool.ShadowReceiver.getReceiveShadows()) {
                this._componentPool.ShadowReceiver.draw(camera);
            } else {
                this._componentPool.Renderable.draw(camera);
            }
            if (this._physics !== null) {
                this._physics.draw(camera);
            }
        }
    }

    destroy () {
        gEngine.Networking.deleteGameObject(this);
        if (this.gameWorld !== null) {
            this.gameWorld.removeGameObject(this);
        }
    }

    destroyAfterSeconds (seconds) {
        setTimeout(() => {
            this.destroy();
        }, seconds * 1000);
    }

    // visibility

    setVisibility (isVisible) {
        this._visible = isVisible;
    }

    getVisibility () {
        return this._visible;
    }

    toggleVisibility () {
        this._visible = !this._visible;
        return this._visible;
    }

    // name

    setName (name) {
        this._name = name;
    }

    getName () {
        return this._name;
    }

    // transform, renderable

    getTransform () {
        return this.getRenderable().getTransform();
    }

    getRenderable () {
        return this._componentPool.Renderable;
    }

    isVisible () {
        return this._visible;
    }
}
