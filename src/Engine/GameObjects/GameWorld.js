import gEngine from "../../GameEngine.js";

export default class GameWorld {
    constructor () {
        this._gameObjectPool = {};
        this._cameraPool = {};

        this._gameObjectRenderQueue = [];
        this._cameraRenderQueue = [];

        this._backgroundColor = [0.7, 0.7, 0.7, 1];
        this._resolvePhysics = false;
    }

    // cameras

    addCamera (camera) {
        camera.gameWorld = this;
        this._cameraPool[camera.kId] = camera;
        this._cameraRenderQueue.push(camera.kId);

        this._cameraRenderQueue.sort((a, b) => {
            return this._cameraPool[a].kCameraZ - this._cameraPool[b].kCameraZ;
        });
    }

    removeCamera (camera) {
        camera.gameWorld = null;
        delete(this._cameraPool[camera.kId]);
        for (let i in this._cameraRenderQueue) {
            if (this._cameraRenderQueue[i] === camera.kId) {
                this._cameraRenderQueue.splice(i, 1);
                return;
            }
        }
    }

    getCameraWithId (id) {
        return this._cameraPool[id];
    }

    // gameobjects

    addGameObject (gameObject) {
        gameObject.gameWorld = this;
        this._gameObjectPool[gameObject.kId] = gameObject;
        this._gameObjectRenderQueue.push(gameObject.kId);
        gameObject.initialize();

        this._gameObjectRenderQueue.sort((a, b) => {
            return this._gameObjectPool[a].kZ - this._gameObjectPool[b].kZ;
        });
    }

    removeGameObject (gameObject) {
        gameObject.gameWorld = null;
        delete(this._gameObjectPool[gameObject.kId]);
        for (let i in this._gameObjectRenderQueue) {
            if (this._gameObjectRenderQueue[i] === gameObject.kId) {
                this._gameObjectRenderQueue.splice(i, 1);
                return;
            }
        }
    }

    getGameObjectWithId (id) {
        return this._gameObjectPool[id];
    }

    findGameObjectsNamed (name) {
        let result = [];
        for (let key in this._gameObjectPool) {
            if (this._gameObjectPool[key].name === name) {
                result.push(this._gameObjectPool[key]);
            }
        }
        return result;
    }

    getGameObjects () {
        return this._gameObjectPool;
    }

    // background color

    setBackgroundColor (color) {
        this._backgroundColor = color;
    }

    getBackgroundColor () {
        return this._backgroundColor;
    }

    // physics

    setResolvePhysics (value) {
        this._resolvePhysics = value;
    }

    getResolvePhysics () {
        return this._resolvePhysics;
    }

    // update, draw

    update () {
        for (let key in this._gameObjectPool) {
            this._gameObjectPool[key].update();
        }

        if (this._resolvePhysics) {
            let keys = Object.keys(this._gameObjectPool);
            for (let i = 0; i < keys.length - 1; i++) {
                for (let j = i + 1; j < keys.length; j++) {
                    let go1 = this._gameObjectPool[keys[i]];
                    let go2 = this._gameObjectPool[keys[j]];
                    if (go1.getPhysics() !== null && go2.getPhysics() !== null) {
                        gEngine.Physics.processObjectObject(go1, go2);
                    }
                }
            }
        }

        for (let i in this._cameraRenderQueue) {
            this._cameraPool[this._cameraRenderQueue[i]].update();
        }
    }

    draw () {
        // clear canvas
        gEngine.Core.clearCanvas(this._backgroundColor);

        // draw all cameras
        for (let i in this._cameraRenderQueue) {
            this.drawCamera(this._cameraPool[this._cameraRenderQueue[i]]);
        }
    }

    drawCamera (camera) {
        camera.setupViewProjection();

        for (let i in this._gameObjectRenderQueue) {
            this._gameObjectPool[this._gameObjectRenderQueue[i]].draw(camera);
        }
    }
}
