import { TiledGameObject } from "../../GameEngine.js";

export default class ParallaxGameObject extends TiledGameObject {
    constructor (renderable, scale, camera, zValue) {
        super(renderable, zValue);
        this._referenceCamera = camera;
        this._cameraWCCenterReference = vec2.clone(this._referenceCamera.getWCCenter());
        this._parallaxScale = 1;
        this.setParallaxScale(scale);
    }

    getParallaxScale () {
        return this._parallaxScale;
    }

    setParallaxScale (scale) {
        if (scale <= 0) {
            this._parallaxScale = 1;
        } else {
            this._parallaxScale = 1 / scale;
        }
    }

    update () {
        super.update();
        let angularComponent = this.getComponent("AngularMovementComponent");
        this._referencePositionUpdate();
        let position = this.getTransform().getPosition();
        vec2.scaleAndAdd(position, position, angularComponent.getCurrentFrontDirection(), angularComponent.getSpeed() * this._parallaxScale);
    }

    _referencePositionUpdate () {
        let deltaT = vec2.fromValues(0, 0);
        vec2.sub(deltaT, this._cameraWCCenterReference, this._referenceCamera.getWCCenter());
        this.setWCTranslationBy(deltaT);
        vec2.sub(this._cameraWCCenterReference, this._cameraWCCenterReference, deltaT);
    }

    setWCTranslationBy (delta) {
        let f = (1 - this._parallaxScale);
        this.getTransform().increaseXPositionBy(-delta[0] * f);
        this.getTransform().increaseYPositionBy(-delta[1] * f);
    }
}
