import gEngine from "../../GameEngine.js";
import { SimpleShader } from "../../GameEngine.js";

export default class TextureShader extends SimpleShader {
    constructor (vertexShaderPath, fragmentShaderPath) {
        // call superclass
        super(vertexShaderPath, fragmentShaderPath);

        // referebce to aTextureCoordinate within the shader
        this._shaderTextureCoordinatesAttribute = null;
        this._samplerReference = null;

        const gl = gEngine.Core.getGL();
        this._samplerReference = gl.getUniformLocation(this._compiledShader, "uSampler");
        this._shaderTextureCoordinatesAttribute = gl.getAttribLocation(this._compiledShader, "aTextureCoordinate");
    }

    activateShader (pixelColor, camera) {
        // call superclass
        super.activateShader(pixelColor, camera);

        // enable texture coordinate array
        const gl = gEngine.Core.getGL();
        gl.bindBuffer(gl.ARRAY_BUFFER, gEngine.VertexBuffer.getGLTextureCoordinatesReference());
        gl.enableVertexAttribArray(this._shaderTextureCoordinatesAttribute);
        gl.vertexAttribPointer(this._shaderTextureCoordinatesAttribute, 2, gl.FLOAT, false, 0, 0);
        gl.uniform1i(this._samplerReference, 0);
    }
}
