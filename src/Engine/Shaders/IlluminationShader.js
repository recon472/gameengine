import gEngine from "../../GameEngine.js";
import {
    LightShader, ShaderMaterial
} from "../../GameEngine.js";

export default class IlluminationShader extends LightShader {
    constructor (vertexShaderPath, fragmentShaderPath) {
        super(vertexShaderPath, fragmentShaderPath);
        const gl = gEngine.Core.getGL();

        // material
        this._material = null;
        this._materialLoader = new ShaderMaterial(this._compiledShader);

        // reference to camera
        this._cameraPosition = null;
        this._cameraPositionReference = gl.getUniformLocation(this._compiledShader, "uCameraPosition");

        // normalMap
        this._normalSamplerReference = gl.getUniformLocation(this._compiledShader, "uNormalSampler");
    }

    activateShader (pixelColor, camera) {
        super.activateShader(pixelColor, camera);

        const gl = gEngine.Core.getGL();
        gl.uniform1i(this._normalSamplerReference, 1);
        this._materialLoader.loadToShader(this._material);
        gl.uniform3fv(this._cameraPositionReference, this._cameraPosition);
    }

    setMaterialAndCameraPosition (material, position) {
        this._material = material;
        this._cameraPosition = position;
    }
}
