import gEngine from "../../GameEngine.js";

export default class SimpleShader {
    constructor (vertexShaderPath, fragmentShaderPath) {
        // instance variables
        this._compiledShader = null;
        this._shaderVertexPositionAttribute = null;
        this._pixelColor = null;
        this._modelTransform = null;
        this._viewProjectionTransform = null;
        this._globalAmbientColor = null;
        this._globalAmbientIntensity = null;

        this.ignoreAmbient = false;

        const gl = gEngine.Core.getGL();

        // load and compile shaders
        this._vertexShader = this._compileShader(vertexShaderPath, gl.VERTEX_SHADER);
        this._fragmentShader = this._compileShader(fragmentShaderPath, gl.FRAGMENT_SHADER);

        // create and link shaders into a program
        this._compiledShader = gl.createProgram();
        gl.attachShader(this._compiledShader, this._vertexShader);
        gl.attachShader(this._compiledShader, this._fragmentShader);
        gl.linkProgram(this._compiledShader);

        // check for error
        if (!gl.getProgramParameter(this._compiledShader, gl.LINK_STATUS)) {
            alert("Error linking shader");
            return null;
        }

        // gets reference to vertex position attribute
        this._shaderVertexPositionAttribute = gl.getAttribLocation(this._compiledShader, "aSquareVertexPosition");

        // get reference to the uniform letiables uPixelColor, uModelTransform, uViewProjTransform
        this._pixelColor = gl.getUniformLocation(this._compiledShader, "uPixelColor");
        this._modelTransform = gl.getUniformLocation(this._compiledShader, "uModelTransform");
        this._viewProjectionTransform = gl.getUniformLocation(this._compiledShader, "uViewProjectionTransform");
        this._globalAmbientColor = gl.getUniformLocation(this._compiledShader, "uGlobalAmbientColor");
        this._globalAmbientIntensity = gl.getUniformLocation(this._compiledShader, "uGlobalAmbientIntensity");
    }

    _compileShader (filePath, shaderType) {
        let shaderSource = null;
        let compiledShader = null;
        const gl = gEngine.Core.getGL();

        shaderSource = gEngine.ResourceMap.retrieveAsset(filePath);

        if (shaderSource === null) {
            alert("WARNING: Loading of:" + filePath + " Failed!");
            return null;
        }

        // create shader based on type
        compiledShader = gl.createShader(shaderType);

        // compile the shader
        gl.shaderSource(compiledShader, shaderSource);
        gl.compileShader(compiledShader);

        // check for errors
        if (!gl.getShaderParameter(compiledShader, gl.COMPILE_STATUS)) {
            alert("A shader compiling error occurred: " +
                gl.getShaderInfoLog(compiledShader));
        }
        return compiledShader;
    }

    // load per objext model transform to VS
    loadObjectTransform (modelTransform) {
        const gl = gEngine.Core.getGL();
        gl.uniformMatrix4fv(this._modelTransform, false, modelTransform);
    }

    activateShader (pixelColor, camera) {
        const gl = gEngine.Core.getGL();
        gl.useProgram(this._compiledShader);
        gl.uniformMatrix4fv(this._viewProjectionTransform, false, camera.getViewportMatrix());
        gl.bindBuffer(gl.ARRAY_BUFFER, gEngine.VertexBuffer.getGLVertexReference());
        gl.vertexAttribPointer(this._shaderVertexPositionAttribute,
            3,          // each vertex element is xyz
            gl.FLOAT,   // data type is float
            false,      // is it normalized vectors
            0,          // bytes to skip in between elements
            0);         // offsets to the first element
        gl.enableVertexAttribArray(this._shaderVertexPositionAttribute);
        gl.uniform4fv(this._pixelColor, pixelColor);

        if (this.ignoreAmbient) {
            gl.uniform4fv(this._globalAmbientColor, [1, 1, 1, 1]);
            gl.uniform1f(this._globalAmbientIntensity, 1);
        } else {
            gl.uniform4fv(this._globalAmbientColor, gEngine.DefaultResources.getGlobalAmbientColor());
            gl.uniform1f(this._globalAmbientIntensity, gEngine.DefaultResources.getGlobalAmbientIntensity());
        }
    }

    getShader () {
        return this._compiledShader;
    }

    cleanUp () {
        const gl = gEngine.Core.getGL();
        gl.detachShader(this._compiledShader, this._vertexShader);
        gl.detachShader(this._compiledShader, this._fragmentShader);
        gl.deleteShader(this._vertexShader);
        gl.deleteShader(this._fragmentShader);
    }
}
