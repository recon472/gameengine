import gEngine from "../../GameEngine.js";
import { SimpleShader } from "../../GameEngine.js";

export default class LineShader extends SimpleShader {
    constructor (vertexShaderPath, fragmentShaderPath) {
        super(vertexShaderPath, fragmentShaderPath);

        this._pointSizeReference = null;

        const gl = gEngine.Core.getGL();

        this._pointSizeReference = gl.getUniformLocation(this._compiledShader, "uPointSize");
        this._pointSize = 1;
    }

    activateShader (pixelColor, camera) {
        super.activateShader(pixelColor, camera);

        const gl = gEngine.Core.getGL();
        gl.uniform1f(this._pointSizeReference, this._pointSize);
        gl.bindBuffer(gl.ARRAY_BUFFER, gEngine.VertexBuffer.getGLLineVertexReference());
        gl.vertexAttribPointer(this._shaderVertexPositionAttribute,
            3,
            gl.FLOAT,
            false,
            0,
            0
        );
        gl.enableVertexAttribArray(this._shaderVertexPositionAttribute);
    }

    setPointSize (width) {
        this._pointSize = width;
    }
}
