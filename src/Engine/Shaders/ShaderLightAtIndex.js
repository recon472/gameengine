import gEngine from "../../GameEngine.js";
import { Light } from "../../GameEngine.js";

export default class ShaderLightAtIndex {
    constructor (shader, index) {
        this._setShaderReferences(shader, index);
    }

    _setShaderReferences (shader, index) {
        const gl = gEngine.Core.getGL();
        this._colorReference = gl.getUniformLocation(shader, "uLights[" + index + "].Color");
        this._positionReference = gl.getUniformLocation(shader, "uLights[" + index + "].Position");
        this._directionReference = gl.getUniformLocation(shader, "uLights[" + index + "].Direction");
        this._nearReference = gl.getUniformLocation(shader, "uLights[" + index + "].Near");
        this._farReference = gl.getUniformLocation(shader, "uLights[" + index + "].Far");
        this._innerReference = gl.getUniformLocation(shader, "uLights[" + index + "].CosInner");
        this._outerReference = gl.getUniformLocation(shader, "uLights[" + index + "].CosOuter");
        this._intensityReference = gl.getUniformLocation(shader, "uLights[" + index + "].Intensity");
        this._dropOffReference = gl.getUniformLocation(shader, "uLights[" + index + "].DropOff");
        this._isOnReference = gl.getUniformLocation(shader, "uLights[" + index + "].IsOn");
        this._lightTypeReference = gl.getUniformLocation(shader, "uLights[" + index + "].LightType");
    }

    loadToShader (camera, light) {
        const gl = gEngine.Core.getGL();
        gl.uniform1i(this._isOnReference, light.isLightOn());
        if (light.isLightOn()) {
            const position = camera.wcPositionToPixel(light.getPosition());
            const near = camera.wcSizeToPixel(light.getNear());
            const far = camera.wcSizeToPixel(light.getFar());
            const color = light.getColor();
            gl.uniform4fv(this._colorReference, color);
            gl.uniform3fv(this._positionReference, vec3.fromValues(position[0], position[1], position[2]));
            gl.uniform1f(this._nearReference, near);
            gl.uniform1f(this._farReference, far);
            gl.uniform1f(this._innerReference, 0.0);
            gl.uniform1f(this._outerReference, 0.0);
            gl.uniform1f(this._intensityReference, light.getIntensity());
            gl.uniform1f(this._dropOffReference, 0);
            gl.uniform1i(this._lightTypeReference, light.getLightType());

            if (light.getLightType() === Light.eLightType.ePointLight) {
                gl.uniform3fv(this._directionReference, vec3.fromValues(0, 0, 0));
            } else {
                const direction = camera.wcDirectionToPixel(light.getDirection());
                gl.uniform3fv(this._directionReference, vec3.fromValues(direction[0], direction[1], direction[2]));
                if (light.getLightType() === Light.eLightType.eSpotlight) {
                    gl.uniform1f(this._innerReference, Math.cos(0.5 * light.getInner()));
                    gl.uniform1f(this._outerReference, Math.cos(0.5 * light.getOuter()));
                    gl.uniform1f(this._dropOffReference, light.getDropOff());
                }
            }
        }
    }

    switchOffLight () {
        const gl = gEngine.Core.getGL();
        gl.uniform1i(this._isOnReference, false);
    }
}
