import {
    SpriteShader, ShaderLightAtIndex
} from "../../GameEngine.js";

export default class ShadowCasterShader extends SpriteShader {
    constructor (vertexShaderPath, fragmentShaderPath) {
        super(vertexShaderPath, fragmentShaderPath);

        this._light = null;

        this._shaderLight = new ShaderLightAtIndex(this._compiledShader, 0);
    }

    activateShader (pixelColor, camera) {
        super.activateShader(pixelColor, camera);
        this._shaderLight.loadToShader(camera, this._light);
    }

    setLight (light) {
        this._light = light;
    }
}
