import {
    SpriteShader, ShaderLightAtIndex
 } from "../../GameEngine.js";

export default class LightShader extends SpriteShader {
    constructor (vertexShaderPath, fragmentShaderPath) {
        super(vertexShaderPath, fragmentShaderPath);

        this._lights = null; // lights from the renderable
        this.kGLSLuLightArraySize = 4; // this has to be the same as in LightFS.glsl
        this._shaderLights = [];
        for (let i = 0; i < this.kGLSLuLightArraySize; i++) {
            let shaderLightAtIndex = new ShaderLightAtIndex(this._compiledShader, i);
            this._shaderLights.push(shaderLightAtIndex);
        }
    }

    setLights (lights) {
        this._lights = lights;
    }

    activateShader (pixelColor, camera) {
        super.activateShader(pixelColor, camera);

        let numberOfLights = 0;
        if (this._lights !== null) {
            while (numberOfLights < this._lights.length) {
                this._shaderLights[numberOfLights].loadToShader(camera, this._lights[numberOfLights]);
                numberOfLights++;
            }
        }

        while (numberOfLights < this.kGLSLuLightArraySize) {
            this._shaderLights[numberOfLights].switchOffLight();
            numberOfLights++;
        }
    }
}
