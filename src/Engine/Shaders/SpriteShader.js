import gEngine from "../../GameEngine.js";
import { TextureShader } from "../../GameEngine.js";

export default class SpriteShader extends TextureShader {
    constructor (vertexShaderPath, fragmentShaderPath) {
        super(vertexShaderPath, fragmentShaderPath);

        this._textureCoordinatesBuffer = null;

        const initTextureCoordinates = [
            1.0, 1.0,
            0.0, 1.0,
            1.0, 0.0,
            0.0, 0.0
        ];

        const gl = gEngine.Core.getGL();
        this._textureCoordinatesBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this._textureCoordinatesBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(initTextureCoordinates), gl.DYNAMIC_DRAW);
    }

    setTextureCoordinates (textureCoordinates) {
        const gl = gEngine.Core.getGL();
        gl.bindBuffer(gl.ARRAY_BUFFER, this._textureCoordinatesBuffer);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, new Float32Array(textureCoordinates));
    }

    activateShader (pixelColor, camera) {
        super.activateShader(pixelColor, camera);

        // bind proper texture coordinate buffer
        const gl = gEngine.Core.getGL();
        gl.bindBuffer(gl.ARRAY_BUFFER, this._textureCoordinatesBuffer);
        gl.vertexAttribPointer(this._shaderTextureCoordinatesAttribute, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(this._shaderTextureCoordinatesAttribute);
    }

    cleanUp () {
        const gl = gEngine.Core.getGL();
        gl.deleteBuffer(this._textureCoordinatesBuffer);

        super.cleanUp();
    }

    setLights (lights) { }
    setMaterialAndCameraPosition (material, position) { }
}
