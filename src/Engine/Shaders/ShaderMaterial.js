import gEngine from "../../GameEngine.js";

export default class ShaderMaterial {
    constructor (illuminationShader) {
        const gl = gEngine.Core.getGL();
        this._kaReference = gl.getUniformLocation(illuminationShader, "uMaterial.Ka");
        this._kdReference = gl.getUniformLocation(illuminationShader, "uMaterial.Kd");
        this._ksReference = gl.getUniformLocation(illuminationShader, "uMaterial.Ks");
        this._shininessReference = gl.getUniformLocation(illuminationShader, "uMaterial.Shininess");
    }

    loadToShader (material) {
        const gl = gEngine.Core.getGL();
        gl.uniform4fv(this._kaReference, material.getAmbient());
        gl.uniform4fv(this._kdReference, material.getDiffuse());
        gl.uniform4fv(this._ksReference, material.getSpecular());
        gl.uniform1f(this._shininessReference, material.getShininess());
    }
}
