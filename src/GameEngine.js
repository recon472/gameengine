// import core singletons

import Core from "./Engine/Core/Engine_Core.js";
import GameLoop from "./Engine/Core/Engine_GameLoop.js";
import Input from "./Engine/Core/Engine_Input.js";
import Networking from "./Engine/Core/Engine_Networking.js";
import Particles from "./Engine/Core/Engine_Particles.js";
import Physics from "./Engine/Core/Engine_Physics.js";
import Textures from "./Engine/Core/Engine_Textures.js";
import Time from "./Engine/Core/Engine_Time.js";
import VertexBuffer from "./Engine/Core/Engine_VertexBuffer.js";

import AudioClips from "./Engine/Core/Resources/Engine_AudioClips.js";
import DefaultResources from "./Engine/Core/Resources/Engine_DefaultResources.js";
import Fonts from "./Engine/Core/Resources/Engine_Fonts.js";
import ResourceMap from "./Engine/Core/Resources/Engine_ResourceMap.js";
import TextFileLoader from "./Engine/Core/Resources/Engine_TextFileLoader.js";
import ProceduralTexture from "./Engine/Core/Resources/Engine_ProceduralTexture.js";
import Random from "./Engine/Core/Engine_Random.js";

// import gEngine classes

import Camera from "./Engine/Cameras/Camera.js";
import Camera_Input from "./Engine/Cameras/Camera_Input.js";
import Camera_Manipulation from "./Engine/Cameras/Camera_Manipulation.js";
import Camera_Transform from "./Engine/Cameras/Camera_Transform.js";
import CameraShake from "./Engine/Cameras/CameraShake.js";
import CameraState from "./Engine/Cameras/CameraState.js";

import NetworkObject from "./Engine/GameObjects/NetworkObject.js";
import GameComponent from "./Engine/GameComponents/GameComponent.js";
import GameObject from "./Engine/GameObjects/GameObject.js";
import GameWorld from "./Engine/GameObjects/GameWorld.js";
import TiledGameObject from "./Engine/GameObjects/TiledGameObject.js";
import ParallaxGameObject from "./Engine/GameObjects/ParallaxGameObject.js";

import Light from "./Engine/Lights/Light.js";
import LightSet from "./Engine/Lights/LightSet.js";

import Particle from "./Engine/Particles/Particle.js";
import ParticleGameObject from "./Engine/Particles/ParticleGameObject.js";
import ParticleEmitter from "./Engine/Particles/ParticleEmitter.js";

import RigidShape from "./Engine/Physics/RigidShape.js";
import RigidShape_Collision from "./Engine/Physics/RigidShape_Collision.js";
import RigidRectangle from "./Engine/Physics/RigidRectangle.js";
import RigidRectangle_Collision from "./Engine/Physics/RigidRectangle_Collision.js";
import RigidCircle from "./Engine/Physics/RigidCircle.js";
import RigidCircle_Collision from "./Engine/Physics/RigidCircle_Collision.js";
import RigidShapeBehavior from "./Engine/Physics/RigidShapeBehavior.js";

import Renderable from "./Engine/Renderables/Renderable.js";
import TextureRenderable from "./Engine/Renderables/TextureRenderable.js";
import TextureRenderable_PixelCollision from "./Engine/Renderables/TextureRenderable_PixelCollision.js";
import SpriteRenderable from "./Engine/Renderables/SpriteRenderable.js";
import SpriteRenderable_PixelCollision from "./Engine/Renderables/SpriteRenderable_PixelCollision.js";
import DirectTextureRenderable from "./Engine/Renderables/DirectTextureRenderable.js";
import SpriteAnimateRenderable from "./Engine/Renderables/SpriteAnimateRenderable.js";
import LightRenderable from "./Engine/Renderables/LightRenderable.js";
import IlluminationRenderable from "./Engine/Renderables/IlluminationRenderable.js";
import LineRenderable from "./Engine/Renderables/LineRenderable.js";
import TextRenderable from "./Engine/Renderables/TextRenderable.js";
import ParticleRenderable from "./Engine/Renderables/ParticleRenderable.js";

import SimpleShader from "./Engine/Shaders/SimpleShader.js";
import TextureShader from "./Engine/Shaders/TextureShader.js";
import SpriteShader from "./Engine/Shaders/SpriteShader.js";
import LineShader from "./Engine/Shaders/LineShader.js";
import LightShader from "./Engine/Shaders/LightShader.js";
import IlluminationShader from "./Engine/Shaders/IlluminationShader.js";
import ShaderLightAtIndex from "./Engine/Shaders/ShaderLightAtIndex.js";
import ShaderMaterial from "./Engine/Shaders/ShaderMaterial.js";
import ShadowCasterShader from "./Engine/Shaders/ShadowCasterShader.js";

import ShadowCaster from "./Engine/Shadows/ShadowCaster.js";
import ShadowReceiver from "./Engine/Shadows/ShadowReceiver.js";
import ShadowReceiver_Stencil from "./Engine/Shadows/ShadowReceiver_Stencil.js";

import BoundingBox from "./Engine/Utils/BoundingBox.js";
import Interpolate from "./Engine/Utils/Interpolate.js";
import InterpolateVec2 from "./Engine/Utils/InterpolateVec2.js";
import NetworkPacket from "./Engine/Utils/NetworkPacket.js";
import NoiseMap from "./Engine/Utils/NoiseMap.js";
import PerlinNoise from "./Engine/Utils/PerlinNoise.js";
import ShakePosition from "./Engine/Utils/ShakePosition.js";
import Transform from "./Engine/Utils/Transform.js";
import CollisionInfo from "./Engine/Utils/CollisionInfo.js";

import Material from "./Engine/Material.js";
import Scene from "./Engine/Scene.js";

// manifests
import ComponentManifest from "./ComponentManifest.js";
import SceneManifest from "./SceneManifest.js";
import GameObjectManifest from "./GameObjectManifest.js";

// export singletons

export default {
    Core: new Core,
    GameLoop: new GameLoop,
    Input: new Input,
    Networking: new Networking,
    Particles: new Particles,
    Physics: new Physics,
    Textures: new Textures,
    Time: new Time,
    VertexBuffer: new VertexBuffer,

    AudioClips: new AudioClips,
    DefaultResources: new DefaultResources,
    Fonts: new Fonts,
    ResourceMap: new ResourceMap,
    TextFileLoader: new TextFileLoader,
    ProceduralTexture: new ProceduralTexture,
    Random: new Random,

    ComponentManifest: ComponentManifest,
    SceneManifest: SceneManifest,
    GameObjectManifest: GameObjectManifest
}

// export classes

export {
    Camera, CameraShake, CameraState,
    GameComponent,
    GameObject, GameWorld, NetworkObject, TiledGameObject, ParallaxGameObject,
    Light, LightSet,
    Particle, ParticleGameObject, ParticleEmitter,
    RigidShape, RigidRectangle, RigidCircle,
    Renderable, DirectTextureRenderable, IlluminationRenderable, LightRenderable, LineRenderable, SpriteAnimateRenderable, SpriteRenderable, TextRenderable, TextureRenderable, ParticleRenderable,
    SimpleShader, TextureShader, SpriteShader, LineShader, LightShader, IlluminationShader, ShaderLightAtIndex, ShaderMaterial, ShadowCasterShader,
    ShadowCaster, ShadowReceiver,
    BoundingBox, Interpolate, InterpolateVec2, NetworkPacket, NoiseMap, PerlinNoise, ShakePosition, Transform, CollisionInfo,
    Material, Scene
}
