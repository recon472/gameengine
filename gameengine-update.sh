#!/bin/bash
mkdir gameengine-update
curl https://bitbucket.org/recon472/gameengine/get/HEAD.zip -o gameengine-update/gameengine.zip
unzip gameengine-update/gameengine.zip -d gameengine-update
rm -r gameengine-update/gameengine.zip
rm -r gameengine-update/*/src/Game
mkdir -p src
cp -r gameengine-update/*/src/* ./src/
rm -r gameengine-update 
