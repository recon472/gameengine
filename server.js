const http = require("http");
const fileSystem = require("fs");
const path = require("path");

const port = "8080";

const server = http.createServer((request, response) => {
    console.log("HTTP - Request for: " + request.url + " by method: " + request.method);

    if (request.method == "GET") {
        var fileUrl = request.url == "/" ? "/index.html" : request.url;
        var filePath = path.resolve("./" + fileUrl);

        fileSystem.exists(filePath, function (exists) {
            if (exists) {
                response.statusCode = 200;
                switch (path.extname(filePath)) {
                    case ".html":
                        response.setHeader("Content-Type", "text/html");
                        break;
                    case ".js":
                        response.setHeader("Content-Type", "text/javascript");
                        break;
                }
                fileSystem.createReadStream(filePath).pipe(response);
            } else {
                response.statusCode = 404;
                response.end();
            }
        });
    }
});

server.listen(port, function () {
    console.log(`HTTP - Server running at http://localhost:${port}`);
});
